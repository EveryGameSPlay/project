﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using TMPro;

namespace EntityCore
{
    [Serializable]
    public class TransformBehaviour // Структура хранящая данные о перемещении сущности
    {
        public float BackSpeed; // Стнадартное значения скорости движения назад
        [NonSerialized]
        public float backSpeed; // Изменяемое значение

        public float ForwardSpeed; // Скорость движения вперед
        [NonSerialized]
        public float forwardSpeed;

        public float SteerSpeed; // Скорость движения
        [NonSerialized]
        public float steerSpeed;

        public Transform[] waypoints; // Точки передвижения

        public void Init()
        {
            backSpeed = BackSpeed;
            forwardSpeed = ForwardSpeed;
            steerSpeed = SteerSpeed;
        }
    }

    [Serializable]
    public class SoulBehaviour 
    {
        
        public float Life;        // Максимальное значение здоровья
        [NonSerialized]
        public float currentLife; // Текущее значение

        public float Mana;        // Мана
        [NonSerialized]
        public float currentMana; // Текущее значение маны

        public float Armour;  // Броня
        [NonSerialized]
        public float currentArmour;  // Текущее значение брони

        public float Strength; // Сила
        [NonSerialized]
        public float currentStrength; // Текущее значение силы

        public float Agility;     // Ловкость 
        [NonSerialized]
        public float currentAgility; // Текщее значение ловкости
        

        public void Init()
        {
            currentLife = Life ;
            currentMana = Mana;
            currentStrength = Strength;
            currentAgility = Agility;
            currentArmour = Armour;
        }

        public void RegenLife(float value) 
        {
            currentLife += value;

            currentLife = Mathf.Clamp(currentLife, 0, Life);
        }

        public virtual void CalculateDamage(Entity.DamageInfo DInfo)
        {
            DInfo.Defender.soulBeh.currentLife    -= DInfo.sumLifeDamage;
            DInfo.Defender.soulBeh.currentMana    -= DInfo.sumManaDamage;
            DInfo.Defender.soulBeh.currentAgility -= DInfo.sumAgilDamage;
        }
        
    }

    [Serializable]
    public class OrganBehaviour // Структура хранящая данные о перемещении сущности
    {
        public float LookSphereRadius; // Радиус сферы коллайдера
        [NonSerialized]
        public float lookSphereRadius; // Радиус сферы на данный момент

        public void Init()
        {
            lookSphereRadius = LookSphereRadius;
        }
    }
}