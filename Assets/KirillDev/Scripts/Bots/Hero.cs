﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hero : Entity {
    
    public override void UpdateValue(int deltaTime, int random) // Обновление информации окружающего мира    (botmanager)
    {
        checkLife = CheckLife();
        if (checkLife == 0)
        {
            status = Status.Death;
            return;
        }

        entityGroup.DivideEntities(LookForEnemy(), this); // Определяем сущностей поблизости

        enemyExist = entityGroup.EnemyExistance(); // Есть ли рядом враги
        
        if (enemyExist)
        {
            Target = entityGroup.GetNearestEnemyEntity(this); // Текущая цель равна близжайшему врагу

            if (status == Status.Command) // Если мы только вышли из под контроля
            {
                status = Status.Agression;
            }
        }
        else
        {
            if (!IsTrooper) // Если не подч., то просто стоим
            {
                Target = null;
                status = Status.Inaction;
            }
        }
    }


    public override void UpdateState()
    {

        base.UpdateState();

        if (IsTrooper)
        {
            CommandTimer();
        }



    }

    #region StateLinks 
    protected override void Inaction() // Мы никого не видим и не слышим 
    {
        if (enemyExist == true) // Мы заметили врага
        {
            status = Status.Anxiety;
        }
    }

    protected override void Alertness() // Что-то случилось , но мы не понимиаем кто , где и когда 
    {

    }

    protected override void Interest() // Нас что-то заинтересовало
    {

    }

    protected override void Anxiety() // Мы видим врага и готовимся к атаке 
    {
        if (enemyExist == false)
        {
            status = Status.Inaction;
        }



        if (Target != null)
        {
            transform.LookAt(Target.transform.position);
        }
    }

    protected override void Agression() // Мы атакуем врагов
    {
        if (IsTrooper)
        {

            agent.SetDestination(Target.transform.position);
        }
        else
        {
            if (enemyExist == false)
            {
                status = Status.Inaction;
            }
        }
    }

    protected override void Escape() // Мы избегаем врагов
    {
        return;
    }

    protected override void Command() // Действия при наличии командира но без приказов
    {
        // Здесь мы ожидаем UpdateValues
        return;
    }

    protected override void Death() // Мы умираем
    {
        base.OnDeathEvent();
    }
    #endregion

    #region Hero Controll
    public void SetControll(Entity target) // Команды от оборотня
    {
        IsTrooper = true;
        Target = target;
        
        LastCommandTime = 0;
    }

    private void CommandTimer() // Счетчик времени с последней команды
    {
        LastCommandTime += Time.deltaTime;
        if (LastCommandTime > 6)
        {
            IsTrooper = false; // Сущность больше не подчиненный
            status = Status.Command;
        }
    }
    #endregion

    public override void UseSkill(int index) // Использование способности
    {
        Skill skill = MainAbilities[index];

        if (skill != null) // Если умение существует
        {
            if (skill.NullableTarget) // Если умению не нужна цель
            {
                if (astatus == AStatus.Idle && animstatus == AnimStatus.Idle) // Если мы можем применить способность
                {
                    print("SkillUse " + gameObject.name);
                    agent.isStopped = true;
                    StartCoroutine(AttackState(1.5f));
                    StartCoroutine(AnimState(1.5f));
                    // anim.SetInteger("State", 3);

                    skill.Use(this, Target);
                }

                return;
            }
            else // Если умению нужна цель
            {
                if (Target != null)
                {
                    if (astatus == AStatus.Idle && animstatus == AnimStatus.Idle) // Если мы можем применить способность
                    {
                        if (skill.DistToHit >= (transform.position - Target.transform.position).magnitude) // Если мы достаем до цели
                        {
                            print("SkillUse " + gameObject.name);
                            agent.isStopped = true;
                            StartCoroutine(AttackState(1.5f));
                            StartCoroutine(AnimState(1.5f));
                            // anim.SetInteger("State", 3);

                            skill.Use(this, Target);
                        }
                        else
                        {
                            agent.isStopped = false;
                            agent.SetDestination(Target.transform.position);
                        }
                    }
                }

            }
        }
    }

}
