﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skeleton : Entity {

    public enum CustomAStatus
    {
        Retreat,    // Отступление
        Offensive,  // Нападение
        Besiegement // Окружение (Осада)
    }

    public CustomAStatus CAstatus;

    private float speed = (2 * Mathf.PI) / 10;
    private int randzerotwo; // Random value between 0 and 2
    private float rotation;

    public override void UpdateValue(int deltaTime,int random) // Обновление информации окружающего мира    (botmanager)
    {
        checkLife = CheckLife();
        if (checkLife == 0)
        {
            status = Status.Death;
            return;
        }
        else if(checkLife == 1) // Мы убегаем
        {
            
        }
        else
        {
            if(CAstatus == CustomAStatus.Retreat)
            {
                behaviorTime = 0;
            }
        }

        randzerotwo = random;

        behaviorTime -= deltaTime;
        if (behaviorTime <= 0)
        {
            int i = randzerotwo;

            if (CAstatus == CustomAStatus.Retreat && checkLife <2) // Если все плохо
            {
                if(i == 0)
                {
                    CAstatus = CustomAStatus.Besiegement;
                }
                else
                {
                    CAstatus = CustomAStatus.Retreat;
                    rotation = 0; // Приравниваем к нулю, чтобы попасть из -90 -- 90 в -60 -- 60
                }
            }
            else
            {
                CAstatus = (i == 0) ? CustomAStatus.Offensive : CustomAStatus.Besiegement;
            }
            
            
            behaviorTime = BehaviorTime;
        }

        entityGroup.DivideEntities(LookForEnemy(), this); // Определяем сущностей поблизости
        enemyExist = entityGroup.EnemyExistance(); // Есть ли рядом враги

        if (enemyExist)
        {
            Target = entityGroup.GetNearestEnemyEntity(this); // Текущая цель равна близжайшему врагу
        }

        if (entityGroup.GetTypeCount(Type) >= 3) 
        {
            groupBaff = true;
        }
        else
        {
            groupBaff = false;
        }

    }


    public override void UpdateState()
    {
        base.UpdateState();
    }

    #region StateLinks 
    protected override void Inaction() // Мы никого не видим и не слышим 
    {
        if (enemyExist == true) // Мы заметили врага
        {
            status = Status.Anxiety;
        }

        anim.SetInteger("State", 1);
    }

    protected override void Alertness() // Что-то случилось , но мы не понимиаем кто , где и когда 
    {

    }

    protected override void Interest() // Нас что-то заинтересовало
    {

    }

    protected override void Anxiety() // Мы видим врага и готовимся к атаке 
    {
        if (enemyExist == false)
        {
            status = Status.Inaction;
        }



        if (Target != null)
        {
            status = Status.Agression;
            transform.LookAt(Target.transform.position);
        }
    }

    protected override void Agression() // Мы атакуем врагов
    {
        if (enemyExist == false)
        {
            status = Status.Inaction;
            return;
        }

        
        Vector3 position = Target.transform.position;
        float FromTarget = (position - transform.position).magnitude;

        if (CAstatus == CustomAStatus.Offensive) 
        {
            if (MainAbilities[0].DistToHit >= FromTarget)
            {

                transform.LookAt(position);
                if (astatus == AStatus.Idle && animstatus == AnimStatus.Idle)
                {
                   
                    agent.isStopped = true;
                    StartCoroutine(AttackState(1.5f));
                    StartCoroutine(AnimState(1.5f));
                    anim.SetInteger("State", 3);

                    MainAbilities[0].Use(this, Target);
                }
            }

            if (animstatus == AnimStatus.Idle)
            {
                agent.isStopped = false;
                anim.SetInteger("State", 2); // Бег
                agent.SetDestination(position);
            }
        }
        else if (CAstatus == CustomAStatus.Besiegement) // Сущность окружает
        {
            if (MainAbilities[0].DistToHit >= FromTarget)
            {
                transform.LookAt(position);
                if (astatus == AStatus.Idle && animstatus == AnimStatus.Idle)
                {
                    agent.isStopped = true;
                    StartCoroutine(AttackState(1.5f));
                    StartCoroutine(AnimState(1.5f));
                    anim.SetInteger("State", 3);

                    MainAbilities[0].Use(this, Target);
                }
            }

            if (animstatus == AnimStatus.Idle)
            {
                agent.isStopped = false;

                
                if(FromTarget > 0.66f * organBeh.lookSphereRadius) // distance > 2/3 from looksphere
                {
                   
                    rotation = randzerotwo > 0 ? 90 : -90;

                    Vector3 offset = (transform.position - position).normalized; // Направление 
                    
                    

                    transform.LookAt(position);
                    anim.SetInteger("State", 2); // Бег

                    Vector3 newPos = transform.position + Quaternion.Euler(0, rotation, 0) * offset * 3.1f;


                    Debug.DrawRay(transform.position, Quaternion.Euler(0, rotation, 0) * offset);
                    agent.SetDestination(newPos);

                }
                else if(FromTarget < 0.33f * organBeh.lookSphereRadius) // distance < 1/3 from looksphere
                {
                    
                    if(checkLife == 1) // Отступаем
                    {
                        CAstatus = CustomAStatus.Retreat;
                        rotation = 0;
                        behaviorTime = BehaviorTime;
                        return;
                    }
                    else if(checkLife == 2) // Нападаем
                    {
                        CAstatus = CustomAStatus.Offensive;
                        behaviorTime = BehaviorTime;
                        return;
                    }
                }
                else if (FromTarget < 0.66f * organBeh.lookSphereRadius && FromTarget >= 0.33f * organBeh.lookSphereRadius) // distance between >= 1/3 and < 2/3  from looksphere
                {
                    if (animstatus == AnimStatus.Idle)
                    {
                        
                        agent.isStopped = false;
                        
                        rotation += speed;

                        if (rotation >= 90 || rotation <= -90)
                        {
                            speed *= -1;
                        }
                        
                        Vector3 offset = (transform.position - position).normalized; // Направление 

                        anim.SetInteger("State", 2); // Бег

                        Vector3 newPos = transform.position + Quaternion.Euler(0, rotation, 0) * offset * 3.1f;

                        Debug.DrawRay(transform.position, Quaternion.Euler(0, rotation, 0) * offset);
                        agent.SetDestination(newPos);
                    }
                }
            }
        }
        else if(CAstatus == CustomAStatus.Retreat) // Убегаем
        {
            if (animstatus == AnimStatus.Idle)
            {
                agent.isStopped = false;
                
                rotation += speed;

                if (rotation >= 60 || rotation <= -60)
                {
                    speed *= -1;
                }
                
                Vector3 offset = (transform.position - position).normalized; // Направление 
                
                anim.SetInteger("State", 2); // Бег

                Vector3 newPos = transform.position + Quaternion.Euler(0, rotation, 0) * offset * 3.1f;
                
                Debug.DrawRay(transform.position, Quaternion.Euler(0, rotation, 0) * offset);
                agent.SetDestination(newPos);
            }
        }
    }

    protected override void Escape() // Мы избегаем врагов
    {
        return;
    }

    protected override void Command() // Действия при наличии командира но без приказов
    {
        // Здесь мы ожидаем UpdateValues
        return;
    }

    protected override void Death() // Мы умираем
    {
        anim.SetInteger("State", 4); // Бег
        base.OnDeathEvent();
    }
    #endregion


    public override void UseSkill(int index) // Использование способности
    {
        Skill skill = MainAbilities[index];

        if (skill != null) // Если умение существует
        {
            if (skill.NullableTarget) // Если умению не нужна цель
            {
                if (astatus == AStatus.Idle && animstatus == AnimStatus.Idle) // Если мы можем применить способность
                {
                    print("SkillUse " + gameObject.name);
                    agent.isStopped = true;
                    StartCoroutine(AttackState(1.5f));
                    StartCoroutine(AnimState(1.5f));
                    // anim.SetInteger("State", 3);

                    skill.Use(this, Target);
                }

                return;
            }
            else // Если умению нужна цель
            {
                if (Target != null)
                {
                    if (astatus == AStatus.Idle && animstatus == AnimStatus.Idle) // Если мы можем применить способность
                    {
                        if (skill.DistToHit >= (transform.position - Target.transform.position).magnitude) // Если мы достаем до цели
                        {
                            print("SkillUse " + gameObject.name);
                            agent.isStopped = true;
                            StartCoroutine(AttackState(1.5f));
                            StartCoroutine(AnimState(1.5f));
                            // anim.SetInteger("State", 3);

                            skill.Use(this, Target);
                        }
                        else
                        {
                            agent.isStopped = false;
                            agent.SetDestination(Target.transform.position);
                        }
                    }
                }

            }
        }
    }

}
