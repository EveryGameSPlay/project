﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WerewolfEntity : Entity {

    public List<WolfEntity> WolfEntities = new List<WolfEntity>(); // Подручные волки
    public List<WolfEntity> Troopers = new List<WolfEntity>(); // Волки у которых есть задания

    
    public enum CustomAStatus
    {
        Sacrifice,  // Жертвоприношение
        Offensive,  // Нападение
    }

    public CustomAStatus CAstatus;

    private Entity nearestWolf;
    private float distance; // Расстояние до близжайшего врага. Если == -1, то значит никого рядом из волков нет

    
    public void GetWolves() // Находим близжайших волков
    {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, organBeh.LookSphereRadius);

        WolfEntities.Clear(); // Чистим списки, чтобы не было повторений и бесконечного добавления
        Troopers.Clear();

        for (int i = 0; i < hitColliders.Length; i++)
        {
            if (hitColliders[i].GetComponent<WolfEntity>())
            {
                WolfEntity wolf = hitColliders[i].GetComponent<WolfEntity>();
                if (!wolf.IsTrooper)
                {
                    WolfEntities.Add(wolf);
                }
                else
                {
                    Troopers.Add(wolf);
                }
            }
        }
    }


    public WolfEntity GetNearestWolfInRange(List<WolfEntity> entities, Entity sender, out float distance) // Получаем близжайшего волка
    {
        if (entities.Count != 0)
        {
            float minDist = -1;
            WolfEntity nearest = null;
            for (int i = 0; i < entities.Count; i++)
            {
                if (entities[i].status != Status.Death)
                {
                    minDist = (sender.transform.position - entities[0].transform.position).magnitude;
                    nearest = entities[i];
                    break;
                }
            }

            if (minDist != -1)
            {
                for (int i = 1; i < entities.Count; i++)
                {
                    WolfEntity wolf = entities[i];
                    if (wolf.status != Status.Death)
                    {
                        if ((wolf.transform.position - sender.transform.position).magnitude < minDist)
                        {
                            minDist = (wolf.transform.position - sender.transform.position).magnitude;
                            nearest = wolf;
                        }
                    }
                }
            }
            else
            {
                distance = -1;
                nearest = null;
            }

            distance = minDist;

            return nearest;
        }
        else
        {
            distance = -1;
            return null;
        }
    }

    public void DivideTargets() // Указываем цели для волков
    {
        int wolfcount = WolfEntities.Count;

       
        for (int w = 0; w < WolfEntities.Count; w++) // Проходимся по всем волкам
        {
            WolfEntity wolf = WolfEntities[w];

            wolf.SetControll(this,Target);
            wolf.AttackEntity();
        }


    }

    //-------------------------------------STATE
    public override void UpdateValue(int deltaTime, int random) // Обновление информации окружающего мира    (botmanager)
    {

        
        checkLife = CheckLife();
        if (checkLife == 0)
        {
            status = Status.Death;
            return;
        }

        GetWolves(); // Ищем всех волков в зоне

        entityGroup.DivideEntities(LookForEnemy(), this); // Определяем сущностей поблизости

        enemyExist = entityGroup.EnemyExistance(); // Есть ли рядом враги
        print(enemyExist);
        if (enemyExist)
        {
            Target = entityGroup.GetNearestEnemyEntity(this); // Текущая цель равна близжайшему врагу
        }
        else
        {
            anim.SetInteger("State", 1);
            Target = null;
        }
        print(Target);
        if (CAstatus == CustomAStatus.Sacrifice)
        {
            nearestWolf = GetNearestWolfInRange(WolfEntities, this, out distance);
        }
    }

   
    #region StateLinks 
    protected override void Inaction() // Мы никого не видим и не слышим 
    {
        if (enemyExist == true) // Мы заметили врага
        {
            status = Status.Anxiety;
        }

        
    }

    protected override void Alertness() // Что-то случилось , но мы не понимиаем кто , где и когда 
    {

    }

    protected override void Interest() // Нас что-то заинтересовало
    {

    }

    protected override void Anxiety() // Мы видим врага и готовимся к атаке 
    {
        if (enemyExist == false)
        {
            status = Status.Inaction;
        }

        

        if (Target != null)
        {
            transform.LookAt(Target.transform.position);

            StartCoroutine(AnimState(3f));
            anim.SetInteger("State", 0);
            
            status = Status.Agression;

        }
    }

    protected override void Agression() // Мы атакуем врагов
    {
        if (enemyExist == false)
        {
            status = Status.Inaction;
            return;
        }



        Vector3 position = Target.transform.position;
        float FromTarget = (transform.position - position).magnitude;

        if (CAstatus == CustomAStatus.Offensive)
        {
            if (checkLife == 2) // У сущности много ХП
            {
                DivideTargets();
                if (MainAbilities[0].DistToHit >= (position - transform.position).magnitude)
                {
                    transform.LookAt(position);
                    if (astatus == AStatus.Idle && animstatus == AnimStatus.Idle)
                    {
                        agent.isStopped = true;
                        StartCoroutine(AttackState(1.5f));
                        StartCoroutine(AnimState(1.5f));
                        anim.SetInteger("State", 3);

                        MainAbilities[0].Use(this, Target);
                    }
                }

                if (animstatus == AnimStatus.Idle)
                {
                    agent.isStopped = false;
                    anim.SetInteger("State", 2); // Бег
                    agent.SetDestination(position);
                }
            }
            else if (checkLife == 1) // У сущности мало ХП
            {

                CAstatus = CustomAStatus.Sacrifice;

                //if (animstatus == AnimStatus.Idle)
                //{

                //    DivideTargets();
                //    if (MainAbilities[0].DistToHit >= FromTarget)
                //    {
                //        transform.LookAt(position);
                //        if (astatus == AStatus.Idle && animstatus == AnimStatus.Idle)
                //        {
                //            agent.isStopped = true;
                //            StartCoroutine(AttackState(1.5f));
                //            StartCoroutine(AnimState(1.5f));
                //            anim.SetInteger("State", 3);

                //            MainAbilities[0].Use(this, Target);
                //        }
                //    }

                //    if (animstatus == AnimStatus.Idle)
                //    {
                //        agent.isStopped = false;
                //        anim.SetInteger("State", 2); // Бег
                //        agent.SetDestination(position);
                //    }


                //}
            }
        }
        else if (CAstatus == CustomAStatus.Sacrifice)
        {
            if (checkLife == 2) // У сущности много ХП
            {
                CAstatus = CustomAStatus.Offensive;
                return;
            }
            else if (checkLife == 1) // У сущности мало ХП
            {
                if (animstatus == AnimStatus.Idle)
                {
                    if (distance != -1 && distance <= FromTarget) // Если волк ближе к сущности чем враг
                    {
                        if (MainAbilities[1].DistToHit >= distance)
                        {
                            transform.LookAt(nearestWolf.transform.position);
                            if (astatus == AStatus.Idle && animstatus == AnimStatus.Idle)
                            {
                                agent.isStopped = true;
                                StartCoroutine(AttackState(1.5f));
                                StartCoroutine(AnimState(1.5f));
                                anim.SetInteger("State", 3);

                                MainAbilities[1].Use(this, nearestWolf);
                            }
                        }

                        if (animstatus == AnimStatus.Idle)
                        {
                            agent.isStopped = false;
                            anim.SetInteger("State", 2); // Бег
                            agent.SetDestination(nearestWolf.transform.position);
                        }
                        
                    }
                    else
                    {
                        DivideTargets();
                        if (MainAbilities[0].DistToHit >= FromTarget)
                        {
                            transform.LookAt(position);
                            if (astatus == AStatus.Idle && animstatus == AnimStatus.Idle)
                            {
                                agent.isStopped = true;
                                StartCoroutine(AttackState(1.5f));
                                StartCoroutine(AnimState(1.5f));
                                anim.SetInteger("State", 3);

                                MainAbilities[0].Use(this, Target);
                            }
                        }

                        if (animstatus == AnimStatus.Idle)
                        {
                            agent.isStopped = false;
                            anim.SetInteger("State", 2); // Бег
                            agent.SetDestination(position);
                        }
                    }

                }
            }
        }
        


    }

    protected override void Escape() // Мы избегаем врагов
    {

        return;
    }

    protected override void Death() // Мы умираем
    {
        anim.SetInteger("State", 4); // Бег
        base.OnDeathEvent();
    }
    #endregion


    public override void UseSkill(int index) // Использование способности
    {
        Skill skill = MainAbilities[index];

        if (skill != null) // Если умение существует
        {
            if (skill.NullableTarget) // Если умению не нужна цель
            {
                if (astatus == AStatus.Idle && animstatus == AnimStatus.Idle) // Если мы можем применить способность
                {
                    print("SkillUse " + gameObject.name);
                    agent.isStopped = true;
                    StartCoroutine(AttackState(1.5f));
                    StartCoroutine(AnimState(1.5f));
                    anim.SetInteger("State", 3);

                    skill.Use(this, Target);
                }

                return;
            }
            else // Если умению нужна цель
            {
                if (Target != null)
                {
                    if (astatus == AStatus.Idle && animstatus == AnimStatus.Idle) // Если мы можем применить способность
                    {
                        if (skill.DistToHit >= (transform.position - Target.transform.position).magnitude) // Если мы достаем до цели
                        {
                            print("SkillUse " + gameObject.name);
                            agent.isStopped = true;
                            StartCoroutine(AttackState(1.5f));
                            StartCoroutine(AnimState(1.5f));
                            anim.SetInteger("State", 3);

                            skill.Use(this, Target);
                        }
                        else
                        {
                            agent.isStopped = false;
                            agent.SetDestination(Target.transform.position);
                        }
                    }
                }

            }
        }
    }
}
