﻿using System.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WolfEntity : Entity {

    public WerewolfEntity werewolf; // Оборотень - вожак

    // Получение урона.
    public override void GetDamageCheck(DamageInfo DInfo) // Действия при получении урона
    {
        if (!IsTrooper)
            status = Status.Agression;
        LastDamager = DInfo.Intiator;
    }

    //-------------------------------------WEREWOLF ACTIONS

    public void AttackEntity() // Указание цели атаки
    {
        status = Status.Agression;
    }

    //-------------------------------------STATE
    public override void UpdateValue(int deltaTime, int random) // Обновление информации окружающего мира    (botmanager)
    {
        checkLife = CheckLife();
        if(checkLife == 0)
        {
            status = Status.Death;
            return;
        }

        entityGroup.DivideEntities(LookForEnemy(), this); // Определяем сущностей поблизости

        enemyExist = entityGroup.EnemyExistance(); // Есть ли рядом враги

        if(entityGroup.GetTypeCount(Type) >= 4)
        {
            groupBaff = true;
        }
        else
        {
            groupBaff = false;
        }

        if (enemyExist)
        {
            Target = entityGroup.GetNearestEnemyEntity(this); // Текущая цель равна близжайшему врагу

            if(status == Status.Command) // Если мы только вышли из под контроля
            {
                status = Status.Agression;
            }
        }
        else
        {
            if (!IsTrooper) // Если не подч., то просто стоим
            {
                Target = null;
                status = Status.Inaction; 
            }
        }

        
    }

    public override void UpdateState()
    {

        base.UpdateState();

        if (IsTrooper)
        {
            CommandTimer();
        }

        
        
    }

    #region StateLinks 
    protected override void Inaction() // Мы никого не видим и не слышим 
    {
        if(enemyExist == true) // Мы заметили врага
        {
            status = Status.Anxiety;
        }
    }

    protected override void Alertness() // Что-то случилось , но мы не понимиаем кто , где и когда 
    {

    }

    protected override void Interest() // Нас что-то заинтересовало
    {
        
    }

    protected override void Anxiety() // Мы видим врага и готовимся к атаке 
    {
        if (enemyExist == false)
        {
            status = Status.Inaction;
        }

        

        if (Target != null)
        {
            transform.LookAt(Target.transform.position);
        }
    }

    protected override void Agression() // Мы атакуем врагов
    {
        if (UnderControll == false)
        {
            if (IsTrooper)
            {

                agent.SetDestination(Target.transform.position);
            }
            else
            {
                if (enemyExist == false)
                {
                    status = Status.Inaction;
                }
            }
        }
        else
        {

        }
    }

    protected override void Escape() // Мы избегаем врагов
    {
        return;
    }
    
    protected override void Command() // Действия при наличии командира но без приказов
    {
        // Здесь мы ожидаем UpdateValues
        return;
    }

    protected override void Death() // Мы умираем
    {
        base.OnDeathEvent();
    }
    #endregion


    #region Werewolf Controll
    public void SetControll(WerewolfEntity werewolfEntity, Entity target) // Команды от оборотня
    {
        IsTrooper = true;
        Target = target;
        werewolf = werewolfEntity; // Присваеваем вожака
        LastCommandTime = 0;
    }

    private void CommandTimer() // Счетчик времени с последней команды
    {
        LastCommandTime += Time.deltaTime;
        if(LastCommandTime > 6)
        {
            IsTrooper = false; // Сущность больше не подчиненный
            status = Status.Command;
        }
    }
    #endregion
}

