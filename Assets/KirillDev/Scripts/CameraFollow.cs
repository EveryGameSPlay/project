﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
// Данный интерфейс наследуют объекты, за которыми может следить камера.
// Также они должны реализовывать OnMouseDown

public interface ICameraFollow 
{
    Transform ICF_Position { get; set; } // Позиция объекта
    Vector3 ICF_Offset { get; set; }     // Отклонение от центра объекта

    int ICF_Priority { get; set; }       // Если приоритет объекта низкий, то другие объекты могут забирать камеру к себе
    bool ICF_Clicked { get; set; }       // Кликнул ли игрок на объект
    
    bool ICF_Event { get; set; }         // Это событие. Оно имеет приоритет выше чем у всего остального
    float ICF_EventTime { get; set; }    // Сколько будет длиться событие

    void ICF_UploadProperties();
}

// Т.к. Unity не отрисовывает интерфейсы, то первоначальные значения храним явно (в файлах)
[CreateAssetMenu(fileName = "New ICF", menuName = "ICF/CameraFollow")]
public class CameraFollowInstance : ScriptableObject
{
    public Vector3 Offset;       // Отклонение от центра объекта
    public int ICF_Priority;     // Если приоритет объекта низкий, то другие объекты могут забирать камеру к себе
    public bool ICF_Event;       // Это событие. Оно имеет приоритет выше чем у всего остального
    public float ICF_EventTime;  // Сколько будет длиться событие
}

public class CameraFollow : OverridableMonoBehaviour, IUpdatable {
    
    public  new Camera camera; // Текущая камера // new для сокрытия Main Camera camera 
    public  ICameraFollow Target; // Основная цель
    private bool EventEnded = true; // Если прошлый объект был собитием, то после его окончания значение станет равно True

    [Range(0,1)]
    public float Smooth; // Коэфицент смягчения передвижения



    private void Start()
    {
        if(camera == null)
        {
            camera = GetComponent<Camera>();
        }
        

        Awake(); // Для 100% добавления в менеджер

        GameManager.gameManager.AddUpdatable(this);
    }
    
    public void UpdateDelta(float deltaTime)
    {
        if (Target != null)
        {
            Vector3 desiredPos = Target.ICF_Position.position + Target.ICF_Offset; // Позиция цели с отклонением
            Vector3 smoothedPos = Vector3.Lerp(transform.position, desiredPos, Smooth); // Делаем переход более плавным
            transform.position = smoothedPos;
        }
    }

    public void UpdateFixedDelta(float deltaTime)
    {
    }

    public void UpdateLateDelta(float deltaTime)
    {
    }

    // Если объект сам вызвал эту функцию и хочет центрировать на себе камеру
    public void SetTarget(ICameraFollow oneOfQueue) 
    {
        if(Target == null)
        {
            Target = oneOfQueue;

            oneOfQueue.ICF_Clicked = false;
            return;
        }

        if (oneOfQueue.ICF_Event) // Это внутриигровое событие
        {
            if (EventEnded) // Если прошлое событие закончилось
            {
                Target = oneOfQueue;
                StartCoroutine(EventTimer(oneOfQueue.ICF_EventTime));
            }
        }
        else if (oneOfQueue.ICF_Clicked == true) // Игрок сам захотел центрировать камеру
        {
            Target = oneOfQueue;

            oneOfQueue.ICF_Clicked = false;
        }
        else if (oneOfQueue.ICF_Priority > Target.ICF_Priority) // Приоритет новой цели больше чем, текущей
        {
            Target = oneOfQueue; // Определяем новую цель
        }
    }

    
    private IEnumerator EventTimer(float time)
    {
        EventEnded = false;
        yield return new WaitForSeconds(time);
        EventEnded = true;
    }

    
}
