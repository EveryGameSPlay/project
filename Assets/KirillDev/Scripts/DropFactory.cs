﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropFactory : MonoBehaviour
{
    public ItemFactory Factory;
}

public abstract class ItemFactory : ScriptableObject
{
    public abstract Item CreateItem();
}

public class ScrollFactory : ItemFactory
{
    public Item item;
    public override Item CreateItem()
    {
        return item;
    }
}
