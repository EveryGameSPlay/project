﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GameManager))]
public class GMEditor : Editor
{
   
    public override void OnInspectorGUI()
    {
        GameManager gm = target as GameManager;
        
        if (GUILayout.Button("Open Editor Window"))
        {
          
            if (GMEditorWindow.AlreadyInit) // Если уже включен
            {
                GMEditorWindow.editorWindow.Close(); // Закрываем предыдущее

                GMEditorWindow.Init(gm.GetActions());
            }
            else
            {
                GMEditorWindow.Init(gm.GetActions());
            }

        }

        DrawDefaultInspector();
    }
    
}



public class GMEditorWindow : EditorWindow
{

    public static bool AlreadyInit; // Уже инициализирован
    public static GMEditorWindow editorWindow; // Ссылка на окно

    public List<SceneView.OnSceneFunc> actions = new List<SceneView.OnSceneFunc>();

    public string AllEnabledString = "Enable all Render";
    public bool AllEnabled = false;

    public string RemoveAllEntitiesStirng = "Remove all Entity";



    public static void Init(List<SceneView.OnSceneFunc> _actions) // Создаем окно
    {
        GMEditorWindow gmew = GetWindow<GMEditorWindow>("Game Manager");

        editorWindow = gmew;
        AlreadyInit = true;

        for (int i = 0; i < editorWindow.actions.Count; i++) // Удаляем старые функции
        {
            SceneView.onSceneGUIDelegate -= editorWindow.actions[i];
        }

        editorWindow.actions = _actions;

    }

    void OnEnable()
    {
        
    }


    void OnDestroy()
    {

        for (int i = 0; i < editorWindow.actions.Count; i++) // Удаляем старые функции
        {
            SceneView.onSceneGUIDelegate -= editorWindow.actions[i];
        }

        AlreadyInit = false;
        editorWindow = null;
        
    }

    void OnGUI()
    {
        GUILayout.BeginHorizontal("box");
        if (GUILayout.Button(AllEnabledString))
        {
            AllEnabled = AllEnabled == false ? true : false; // Меняем на противоположное
            EnableDisableAllRender();
        }

        if (GUILayout.Button(RemoveAllEntitiesStirng)) // Убираем всех сущностей в состояние покоя
        {
            GameManager.gameManager.MainEntityManager.RemoveAll();
            GameManager.gameManager.solid.RemoveAll();
        }

        GUILayout.EndHorizontal();
        
    }

    

    void EnableDisableAllRender() // Включаем или выключаем всю отрисовку
    {
        if (!AllEnabled) // Если мы выключаем
        {
            for (int i = 0; i < editorWindow.actions.Count; i++) // Удаляем старые функции
            {
                SceneView.onSceneGUIDelegate -= editorWindow.actions[i];
            }

            editorWindow.AllEnabledString = "Enable all Render";
        }
        else
        {

            for (int i = 0; i < editorWindow.actions.Count; i++) // Добавляем новые функции
            {
                SceneView.onSceneGUIDelegate += editorWindow.actions[i];
            }

            editorWindow.AllEnabledString = "Disable all Render";
        }
    }
}
