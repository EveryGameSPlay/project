﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using TMPro;

using EntityCore;


public class EntityHistory
{
    public Stack<EntityMemento> History { get; private set; }

    public EntityHistory()
    {
        History = new Stack<EntityMemento>();
    }
}

public class EntityMemento
{
    public bool Controllable; // Может ли игрок управлять существом 

    public float Life;    // Хп
    public float Mana;    // Мана
    public float Agility; // Ловкость 

    public Vector3 Position;


    public EntityMemento(float life, float mana, float agility,Vector3 position)
    {
        Life = life;
        Mana = mana;
        Agility = agility;
        Position = position;
    }
}

[CreateAssetMenu(fileName = "New Entity", menuName = "Info/Entity")]
public class EntityInfo : ScriptableObject
{
    public Sprite Avatar;

    //(Пока рано)
    //public string Short; // Краткая информация о сущности

}

public class EntityGroup 
{
    public List<Entity> Friends = new List<Entity>(); // Список союзников
    public List<Entity> Enemies = new List<Entity>(); // Список врагов

    public virtual void DivideEntities(List<Entity> entities, Entity sender) // Распределяем полученные сущности по командам (Друг - Враг)
    {
        Friends.Clear();
        Enemies.Clear();
        for(int i = 0; i < entities.Count; i++)
        {
            if (entities[i].status != Entity.Status.Death) // Если сущность не мертва
            {
                if (entities[i].Team == sender.Team) 
                {
                    
                    Friends.Add(entities[i]);
                }
                else
                {
                    Enemies.Add(entities[i]);
                }
            }
        }
    }

    public virtual bool EnemyExistance() // Есть ли рядом враги
    {
        
        if (Enemies.Count != 0)
            return true;

        return false;
    }

    public virtual int GetTypeCount(string type) // Проверяем совместимость типов в списке Friends
    {
        int count = 0;
        for (int i = 0; i < Friends.Count; i++)
        {
            if (Friends[i].Type == type) // Если типы совпадают
            {
                count++;
            }
        }

        return count;
    }

    public virtual Entity GetNearestEnemyEntity(Entity sender) // Получаем близжайшего врага (работает неправильно, но подходит для рандома)
    {
        if (Enemies.Count != 0)
        {
            float minDist = (sender.transform.position - Enemies[0].transform.position).magnitude;
            Entity nearest = Enemies[0];

            for (int i = 1; i < Enemies.Count; i++)
            {
                if ((Enemies[i].transform.position - sender.transform.position).magnitude < minDist)
                {
                   // minDist = (Enemies[i].transform.position - sender.transform.position).magnitude;
                    nearest = Enemies[i];
                }
            }

            return nearest;
        }
        else
        {
            return null;
        }
    }

    public virtual Entity GetNearestEnemyEntity_True(Entity sender) // Получаем близжайшего врага (работает правильно)
    {
        if (Enemies.Count != 0)
        {
            float minDist = (sender.transform.position - Enemies[0].transform.position).magnitude;
            Entity nearest = Enemies[0];

            for (int i = 1; i < Enemies.Count; i++)
            {
                if ((Enemies[i].transform.position - sender.transform.position).magnitude < minDist)
                {
                    // minDist = (Enemies[i].transform.position - sender.transform.position).magnitude;
                    nearest = Enemies[i];
                }
            }

            return nearest;
        }
        else
        {
            return null;
        }
    }

    public virtual Entity GetNearestEntityInRange(List<Entity> entities, Entity sender)
    {
        if (entities.Count != 0)
        {
            float minDist = (sender.transform.position - entities[0].transform.position).magnitude;
            Entity nearest = entities[0];

            for (int i = 1; i < Enemies.Count; i++)
            {
                if ((entities[i].transform.position - sender.transform.position).magnitude < minDist)
                {
                    nearest = entities[i];
                }
            }

            return nearest;
        }
        else
        {
            return null;
        }
    }
    
}


public class Entity : OverridableMonoBehaviour, ICameraFollow {

    public class DamageInfo
    {

        // Эти два поля обязательны к заполнению (Entity)
        public Entity Intiator; // Ссылка на атакующего
        public Entity Defender; // Ссылка на обороняющегося

        public float LifeDamage { get { return lifedamage; } set { sumLifeDamage = lifedamage = value; } } // Изначальный урон
        public float lifedamage;
        public float ManaDamage { get { return manadamage; } set { sumManaDamage = manadamage = value; } }
        public float manadamage;
        public float AgilDamage { get { return agildamage; } set { sumAgilDamage = agildamage = value; } }
        public float agildamage;

        public float sumLifeDamage; // Окончательный урон
        public float sumManaDamage;
        public float sumAgilDamage;

        public void CalculateDamage() 
        {
            Defender.soulBeh.currentLife -= sumLifeDamage;
            Defender.soulBeh.currentMana -= sumManaDamage;
            Defender.soulBeh.currentAgility -= sumAgilDamage;
        }
    }

    protected bool ManagerRegistered; // Управляет ли сущностью менеджер

    public bool DEBUG_MOD;
    public PhysicsDebugPreview DebugLines; // Линии сфер

    public bool UnderControll; // Сущностью непосредственно управляет игрок
    public bool GroupControll; // Сущность в группе игрока
    public bool Controllable;  // Может ли сущность быть подконтрольна игроку

    [Space]
    public EntityInfo Info; // Информация о сущности
    
    public TransformBehaviour transformBeh; // Модель поведения перемещения
    public SoulBehaviour soulBeh;   // модель поведения жизни
    public OrganBehaviour organBeh; // Модель поведения органов зрения

    public EntityHistory History; // Последние состояния сущности 

    public delegate void DeathEvent();
    public event DeathEvent deathEvent; // Собыитие при смерти персонажа

    public delegate void DamageEvent(DamageInfo DInfo);
    public event DamageEvent damageEvent;

    public delegate void FloatValue(ref float value, Entity sender);
    public event FloatValue Lifereg;    // Когда сущность хилится
    public event FloatValue Manareg;    // Когда сущность пополняет ману
    public event FloatValue Agilreg;    // Когда сущность восстанавливает выносливость

    public delegate void DamageInfoValue(DamageInfo DInfo, Entity sender);
    public event DamageInfoValue LifeDamage; // Когда сущность получает урон
    public event DamageInfoValue ManaDamage; // Когда у сущности сжигают ману
    public event DamageInfoValue AgilDamage; // Когда у сущности сжигают ловкость

    [Space][Header("Abilities")]
    public Skill[] MainAbilities = new Skill[5]; // Активные способности сущности 
    public Skill[] BornAbilities = new Skill[2]; // Врожденные способности сущности (зависят от класса существа) 
    public List<Skill> BaffAbilities = new List<Skill>(); // Эффекты накладываемые на сущность

    public Entity Target;      // Последняя выделенная цель
    public Entity LastDamager; // Последняя сущность, которая нанесла урон this

    #region Runtime Values
    #region ENUMS
    public enum Status
    {
        Inaction,
        Alertness,
        Interest,
        Anxiety,
        Agression,
        Escape,
        Death,
        Command
    }
    public enum AStatus
    {
        Idle,
        InAttack
    }
    public enum AnimStatus // Накладывается только на те анимации, которые нельзя пропускать
    {
        Idle,
        Anim
    }
    #endregion

    [Space][Header("Runtime Values")]
    public Status status;
    public AStatus astatus;
    public int BehaviorTime; 
    protected int behaviorTime; // Таймер поведения во время атаки
    public AnimStatus animstatus;

    [NonSerialized]public EntityGroup entityGroup = new EntityGroup(); // Списки сущностей

    public string Type; // Тип объекта
    public string Team; // Идентификатор команды
    public bool IsTrooper; // Могут ли этой сущностью командовать другие
    protected float LastCommandTime; // Время прошедшее с момента последней команды

    
    #region Function Value
    // CheckLife
    protected int  checkLife;  // Какой у нас уровень хп
    // EnemyExistanse
    protected bool enemyExist; // Cуществуют ли враги около нас
    // Death
    protected bool deathing;   // Мы умерли ?
    // GroupBaff
    protected bool groupBaff;  // Действует ли группавая способность
    #endregion
    #endregion
    
    #region Cached Values
    public float Height; // Расстояние от земли до центра сущности
    public Transform[] waypoints;
    protected int pointindex; // Текущий WayPoint

    [NonSerialized] public NavMeshAgent agent;
    [NonSerialized] public Animator anim;

    
    #endregion

    //---------------------------------------------- INTERFACE SETTINGS

    #region ICameraFollow
    public Transform ICF_Position { get; set; }
    public Vector3 ICF_Offset { get; set; }
    public int ICF_Priority { get; set; }

    public bool ICF_Clicked { get; set; }
    public bool ICF_Event { get; set; }
    public float ICF_EventTime { get; set; }

    public CameraFollowInstance ICF_Instance; // Дефолтные настройки, которые мы загружаем через инспектор
    #endregion
    
    //-------------------------------------------FUNCTIONS
    #region Functions
    protected virtual List<Entity> LookForEnemy() // Список видимых врагов
    {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, organBeh.LookSphereRadius);
        List<Entity> entities = new List<Entity>(hitColliders.Length);

        for(int i = 0; i < hitColliders.Length; i++)
        {
            if (hitColliders[i].GetComponent<Entity>())
            {
                entities.Add(hitColliders[i].GetComponent<Entity>());
            }
        }

        return entities;
    }

    protected virtual int CheckLife() // 0 - death , 1 - low hp , 2 - high hp
    {
        if (soulBeh.currentLife <= 0) return 0;
        if (soulBeh.currentLife / soulBeh.Life <= 0.5f) return 1;
        return 2;
    }

    protected virtual bool EnemyExistanse() // Присутствуют ли враги около нас 
    {
        // Overlap
        return false; // Врагов поблизовсти нет
    }

    protected virtual void WalkToPoint()  // Сheckpoint - waypoint
    {
        if ((waypoints[pointindex].position - transform.position).magnitude < 3f) pointindex++;
        if (pointindex >= waypoints.Length) pointindex = 0;
        agent.SetDestination(waypoints[pointindex].position);
    }

    protected virtual void WalkToTarget() // Идем к цели
    {
        
    }
    #endregion


    // Use this for initialization
    protected virtual void Start ()
    {
        StartCoroutine(RegistChecker());

        ICF_UploadProperties();
        RestoreStats();

        //DEBUG_MOD = true;
        DebugLines = gameObject.AddComponent<PhysicsDebugPreview>();
        DebugLines.SetParent(this);
        DebugLines.castType = PhysicsDebugPreview.PhysicsType.OverlapSphere;
        DebugLines.preview = RotaryHeart.Lib.PhysicsExtension.Physics.PreviewCondition.Game;

        
        if (agent == null)
            agent = GetComponent<NavMeshAgent>();
         
        if (anim == null)
            anim = GetComponent<Animator>();

        for (int i = 0; i < MainAbilities.Length; i++)
        {
            if (MainAbilities[i] != null)
                MainAbilities[i].Subscribe(this);
        }
        for (int i = 0; i < BornAbilities.Length; i++)
        {
            if (BornAbilities[i] != null)
                BornAbilities[i].Subscribe(this);
        }

        organBeh.lookSphereRadius = organBeh.LookSphereRadius;

        Awake(); // Для 100% добавления в менеджер

    }

    public virtual void UpdateValue(int deltaTime,int random) // Обновление информации окружающего мира    (botmanager)
    {
        
    }

    public virtual void UpdateState() // Выполнение состояний    (botmanager)
    {
       
        if (!UnderControll) // Если игрок не управляет сущностью
        {
            switch (status)
            {
                case Status.Inaction: Inaction(); return;
                case Status.Alertness: Alertness(); return;
                case Status.Interest: Interest(); return;
                case Status.Anxiety: Anxiety(); return;
                case Status.Agression: Agression(); return;
                case Status.Escape: Escape(); return;
                case Status.Death: Death(); return;
                case Status.Command: Command(); return;
            }
        }
    }

    public override void UpdateMe()
    {
        if(DEBUG_MOD == true)
        {
            ICF_UploadProperties();
        }
    }

    public override void LateUpdateMe()
    {
        if (DEBUG_MOD == true)
        {
            DebugLines.RenderLines();
          
        }

    }

    //---------------------------------------------STATE LINKS
    #region StateLinks 
    protected virtual void Inaction() // Мы никого не видим и не слышим 
    {
       
    }

    protected virtual void Alertness() // Что-то случилось , но мы не понимиаем кто , где и когда 
    {

    }

    protected virtual void Interest() // Нас что-то заинтересовало
    {

    }

    protected virtual void Anxiety() // Мы видим врага и готовимся к атаке 
    {

    }

    protected virtual void Agression() // Мы атакуем врагов
    {

    }

    protected virtual void Escape() // Мы избегаем врагов
    {
       
        return;
    }

    protected virtual void Command() // Действия с командиром
    {

    }

    protected virtual void Death() // Мы умираем
    {
        deathEvent();
    }
    #endregion

    //---------------------------------------------- EVENTS
    protected virtual void OnDeathEvent()
    {
        deathEvent?.Invoke();
    }
    
    // Восстановление
    public virtual void LifeR(float value)
    {
        Lifereg?.Invoke(ref value, this);
        soulBeh.RegenLife(value);
    }

    public virtual void ManaR(float value)
    {
        Manareg?.Invoke(ref value,this);
    } 

    public virtual void AgilR(float value)
    {
        Agilreg?.Invoke(ref value,this);
    }
    

    // Получение урона.
    public virtual void GetDamageCheck(DamageInfo DInfo) // Действия при получении урона
    {
        damageEvent?.Invoke(DInfo);
        
        UIController.UI.AddPopup(DInfo.lifedamage, transform.position);
        status = Status.Agression;
        LastDamager = DInfo.Intiator;
    }

    /// <summary>
    /// True - пустое событие
    /// </summary>
    public virtual bool CheckGetDamageEvent()
    {
        if (damageEvent == null)
            return true;

        return false;
    }

    public virtual void GetLifeDamage(DamageInfo DInfo)
    {
        GetDamageCheck(DInfo);
        LifeDamage?.Invoke(DInfo,this);
        soulBeh.CalculateDamage(DInfo);
    }

    public virtual void GetManaDamage(DamageInfo DInfo)
    {
        GetDamageCheck(DInfo);
        ManaDamage?.Invoke(DInfo,this);
        soulBeh.CalculateDamage(DInfo);
    }

    public virtual void GetAgilDamage(DamageInfo DInfo)
    {
        GetDamageCheck(DInfo);
        AgilDamage?.Invoke(DInfo,this);
        soulBeh.CalculateDamage(DInfo);
    }

    //---------------------------------------------- METHODS

    public virtual void RegistByManager(bool registration) // Регестрируем бота в EntityManager
    {
        ManagerRegistered = registration;
    }
    public virtual IEnumerator RegistChecker()
    {
        yield return new WaitForSeconds(0.4f);
        if(ManagerRegistered == false)
        {
            SolidEntityManager.SEM.AddEntity(this);
        }
    }

    public virtual IEnumerator AnimState(float time)
    {
        animstatus = AnimStatus.Anim;
        yield return new WaitForSeconds(time);
        animstatus = AnimStatus.Idle;
    }
    public virtual IEnumerator AttackState(float time)
    {
        astatus = AStatus.InAttack;
        yield return new WaitForSeconds(time);
        astatus = AStatus.Idle;
    }

    public virtual void UseSkill(int index) // Использование способности
    {
        Skill skill = MainAbilities[index];

        if (skill != null) // Если умение существует
        {
            if (skill.NullableTarget) // Если умению не нужна цель
            {
                if (astatus == AStatus.Idle && animstatus == AnimStatus.Idle) // Если мы можем применить способность
                {
                    print("SkillUse " +gameObject.name);
                    agent.isStopped = true;
                    StartCoroutine(AttackState(1.5f));
                    StartCoroutine(AnimState(1.5f));
                    // anim.SetInteger("State", 3);

                    skill.Use(this, Target);
                }

                return;
            }
            else // Если умению нужна цель
            {
                if (Target != null)
                {
                    if (astatus == AStatus.Idle && animstatus == AnimStatus.Idle) // Если мы можем применить способность
                    {
                        if (skill.DistToHit >= (transform.position - Target.transform.position).magnitude) // Если мы достаем до цели
                        {
                            print("SkillUse " +gameObject.name);
                            agent.isStopped = true;
                            StartCoroutine(AttackState(1.5f));
                            StartCoroutine(AnimState(1.5f));
                            // anim.SetInteger("State", 3);

                            skill.Use(this, Target);
                        }
                        else
                        {
                            agent.isStopped = false;
                            agent.SetDestination(Target.transform.position);
                        }
                    }
                }
                
            }
        }
    }

    public virtual void MoveToPosition(Vector3 pos)
    {
        if (astatus == AStatus.Idle && animstatus == AnimStatus.Idle) // Если мы можем применить способность
        {
            agent.isStopped = false;
            agent.SetDestination(pos);
        }
    }

    // Устанавливаем значения для CameraController
    public void ICF_UploadProperties()
    {
        ICF_Event = ICF_Instance.ICF_Event;
        ICF_EventTime = ICF_Instance.ICF_EventTime;

        ICF_Position = transform;

        ICF_Offset = ICF_Instance.Offset;
        ICF_Priority = ICF_Instance.ICF_Priority;
    }

    //---------------------------------------------- FUNCTIONS

    public virtual void SetWayPoints(Transform[] wayArray) // Заменяем waypoints
    {
        waypoints = wayArray;
    }

    public virtual void RestoreStats()
    {
        organBeh.Init();
        soulBeh.Init();
        transformBeh.Init();

        soulBeh.currentLife = soulBeh.Life;
        soulBeh.currentMana = soulBeh.Mana;
        soulBeh.currentAgility = soulBeh.Agility;
        soulBeh.currentArmour = soulBeh.Armour;
    }

    public virtual void Print(string message)
    {
        print(message);
    }

    // Сохранение состояния
    public EntityMemento SaveState() 
    { 
        return new EntityMemento(soulBeh.Life, soulBeh.Mana, soulBeh.Agility,transform.position);
    }

    // Восстановление состояния
    public void RestoreState(EntityMemento memento) 
    {
        soulBeh.Life = memento.Life;
        soulBeh.Mana = memento.Mana;
        soulBeh.Agility = memento.Agility;
        transform.position = memento.Position;
    }



}
