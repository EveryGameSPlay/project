﻿using System;
using System.Collections;
using System.Collections.Generic;
using U = UnityEngine;
using System.Threading.Tasks;




public class EntityManager : OverridableMonoBehaviour, IUpdatable
{
    
    public List<Entity> Mybots = new List<Entity>();
    
    protected bool valueU; // U - update
    public int valueUpdateRate; // как часто будет обновлятся Values на всех ботах
    
    public virtual void Start()
    {
        valueU = true; // Начинаем обновлять со старта
        base.Awake();

        for(int i =0; i < Mybots.Count; i++) // Регестрируем ботов
        {
            if (Mybots[i] != null)
            {
                Mybots[i].RegistByManager(true);
            }
            else
            {
                Mybots.RemoveAt(i);
            }
        }
    }
    
   
    public void UpdateDelta(float deltaTime)
    {
        if (valueU == true)
        {

            for (int i = 0; i < Mybots.Count; i++)
            {
                Entity entity = Mybots[i];
                if (!entity.gameObject.activeSelf || entity == null)
                {
                    RemoveEntity(entity);
                }
                else
                {
                    int random = U.Random.Range(0, 2);
                    entity.UpdateValue(valueUpdateRate, random);
                    entity.UpdateState();
                }

            }
            ValueUpdateTimer();
        }
        else
        {
            for (int i = 0; i < Mybots.Count; i++)
            {
                Entity entity = Mybots[i];
                if (!entity.gameObject.activeSelf || entity == null)
                {
                    RemoveEntity(entity);
                }
                else
                {
                    entity.UpdateState();
                }
            }
        }
    }

    public void UpdateFixedDelta(float deltaTime)
    {
        
    }

    public void UpdateLateDelta(float deltaTime)
    {
        
    }

    protected async void ValueUpdateTimer()
    {
        valueU = false;
        await Task.Delay(valueUpdateRate);
        valueU = true;

    }

    public void AddEntity(Entity entity)
    {
        Mybots.Add(entity);
    }

    public void RemoveEntity(Entity entity) // Удаляем одну сущность и добавляем в пулл
    {
        
        Mybots.Remove(entity);

        if(entity != null)
        {
            ObjectPoolSystem.PoolSys.AddObj(entity,ObjectType.Entity,entity.Type);
        }
    }

    public void RemoveAll() // Удаляем всех сущностей из списка и добавляем их в пулл
    {
        for (int i = Mybots.Count-1; i>=0; i--)
        {
            Entity entity = Mybots[i];

            if (entity == null)
            {
                Mybots.Remove(entity);
            }
            else
            {
                entity.gameObject.SetActive(false);

                Mybots.Remove(entity);
                ObjectPoolSystem.PoolSys.AddObj(entity, ObjectType.Entity, entity.Type);
            }
        }
    }

    
}
