﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ObjectType
{
    Bullet,
    Effect,
    Entity
}

[Serializable]
public class DynamicObject { // Like Item class

    public OverridableMonoBehaviour Prefab;

    public ObjectType objType;
    public string Identifier; //(Muzzle Flash, Impact, и т.д.)

}
