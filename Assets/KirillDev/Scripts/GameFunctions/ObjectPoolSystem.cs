﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolSystem : MonoBehaviour {

    /*
        Структура : Делаем запрос по типу объекта, а потом по идентификатору
        Entity.Type должен совпадать с идентификатором
    */

    // Класс "бассейнов"
    [System.Serializable]
    public class Pool
    {
        public DynamicObject prefab;

        public int ObjectCount; // Кол-во объектов в "бассейне"
    }


    public List<Pool> Pools; // Список пресетов для инициализации

    public int MinObjectCount; // Минимальное значение объектов в пуле во время игры 

    public Dictionary<ObjectType, Dictionary<string, Queue<OverridableMonoBehaviour>>> PoolObj; // Переменная, хранящая созданные "Объекты"

    public static ObjectPoolSystem PoolSys; // singleton

    void Awake()
    {

        GameObject Parent = GameObject.Find("Pool");
        PoolSys = this;

        PoolObj = new Dictionary<ObjectType, Dictionary<string, Queue<OverridableMonoBehaviour>>>(); // Инициализируем коллекцию

        // "Пробегаемся" по всем "бассейнам" в списке
        foreach (Pool Pool in Pools)
        {

            // НОВЫЙ БЛОК КОДА! Создание объектов в с припиской (Type), предварительно проверив на существование
            if (!GameObject.Find(Pool.prefab.objType + "(Type)"))
            {
                GameObject obj = new GameObject();
                obj.name = Pool.prefab.objType + "(Type)";
                obj.transform.parent = Parent.transform;
            }


            // Инициализируем лист, для временного хранения созданных объектов
            Queue<OverridableMonoBehaviour> list = new Queue<OverridableMonoBehaviour>();
            // Инициализируем словарь, для временного хранения пар { идентификатор : список объктов }
            Dictionary<string, Queue<OverridableMonoBehaviour>> identifierList = new Dictionary<string, Queue<OverridableMonoBehaviour>>();

            for (int i = 0; i < Pool.ObjectCount; i++)
            {

                // Создаём на сцене объект
                OverridableMonoBehaviour ItemObj = Instantiate(Pool.prefab.Prefab, transform.position, transform.rotation);

                // НОВЫЙ БЛОК КОДА!
                if (!GameObject.Find(Pool.prefab.Identifier + "Identifier"))
                {
                    GameObject obj = new GameObject();
                    obj.name = Pool.prefab.Identifier + "Identifier";
                    obj.transform.parent = GameObject.Find(Pool.prefab.objType + "(Type)").transform;
                }

                ItemObj.transform.parent = GameObject.Find(Pool.prefab.Identifier + "Identifier").transform;

                // Деактивируем его
                ItemObj.gameObject.SetActive(false);
                // Добавили во временный лист
                list.Enqueue(ItemObj);
            }
            // Запись пары { идентификатор : список объктов } во временный лист
            identifierList.Add(Pool.prefab.Identifier, list);

            if (!PoolObj.ContainsKey(Pool.prefab.objType))
            {
                PoolObj.Add(Pool.prefab.objType, identifierList);
            }
            else
            {
                PoolObj[Pool.prefab.objType].Add(Pool.prefab.Identifier, list);
            }

        }
    }

    public OverridableMonoBehaviour GetObj(ObjectType type, string identifier)
    {
        int count = 4;

        if (PoolObj[type][identifier].Count <= count)
        {
            OverridableMonoBehaviour prefab = PoolObj[type][identifier].Peek();

            for (int i = 0; i < count; i++)
            {

                // Создаём на сцене объект
                OverridableMonoBehaviour ItemObj = Instantiate(prefab, transform.position, transform.rotation);

                ItemObj.transform.parent = GameObject.Find(identifier + "Identifier").transform;

                // Деактивируем его
                ItemObj.gameObject.SetActive(false);

                PoolObj[type][identifier].Enqueue(ItemObj);
            }
            print("creating");
        }

        return PoolObj[type][identifier].Dequeue();
    }

    public void AddObj(OverridableMonoBehaviour omb,ObjectType type, string identifier)
    {
        omb.gameObject.SetActive(false);
        PoolObj[type][identifier].Enqueue(omb);

    }

}
