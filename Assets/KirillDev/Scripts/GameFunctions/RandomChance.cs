﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public interface IRandomChance
{
    int IRC_Chance { get; set; } // Вероятность выпадения
}

public static class RandomChance
{
    public static int RandomItem(List<IRandomChance> IRC_items) // Случайный предмет из списка
    {
        var range = 0;
        for (int i = 0; i < IRC_items.Count; i++)
            range += IRC_items[i].IRC_Chance;

        var rand = Random.Range(0, range);
        var top = 0;

        for (int i = 0; i < IRC_items.Count; i++)
        {
            top += IRC_items[i].IRC_Chance;
            if (rand < top)
                return i;
        }

        return 0;
    }

    public static int RandomItem(IRandomChance[] IRC_items) // Случайный предмет из массива
    {
        var range = 0;
        for (int i = 0; i < IRC_items.Length; i++)
            range += IRC_items[i].IRC_Chance;

        var rand = Random.Range(0, range);
        var top = 0;

        for (int i = 0; i < IRC_items.Length; i++)
        {
            top += IRC_items[i].IRC_Chance;
            if (rand < top)
                return i;
        }

        return 0;
    }

    public static bool RandomBool(int chance)
    {
        var rand = Random.Range(0, chance);
        
        if (rand < chance)
            return true;

        return false;
    }
}



