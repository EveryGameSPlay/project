﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public interface IUpdatable
{
    void UpdateDelta(float deltaTime);

    void UpdateFixedDelta(float deltaTime);

    void UpdateLateDelta(float deltaTime);

}

public class GameManager : OverridableMonoBehaviour {

    public static GameManager gameManager; 
    
    public HeroTeam  heroTeam;
    public EnemyTeam enemyTeam;

    public List<IUpdatable> Updatables = new List<IUpdatable>();

    #region MainComponents
    public EntityManager MainEntityManager; // Здесь управляются все сущности
    public SolidEntityManager solid; // Только до момента, пока не будет доделана спавн-ситема

    public PlayerController playerController;
    public UIController uIController;

    public ObjectPoolSystem objectPoolSystem;
    #endregion

    // Use this for initialization
    void Start () {
        base.Awake();

        Init();

    }

    public void Init()
    {
        if (gameManager == null)
            gameManager = this;

        if (MainEntityManager == null)
        {
            MainEntityManager = gameObject.GetComponent<EntityManager>();

            IUpdatable up = MainEntityManager as IUpdatable;
            if (up != null)
                Updatables.Add(up);
        }

        if(solid == null)
        {
            solid = gameObject.GetComponent<SolidEntityManager>();
            
        }

        if (playerController == null)
        {
            playerController = gameObject.GetComponent<PlayerController>();

            IUpdatable up = playerController as IUpdatable;
            if (up != null)
                Updatables.Add(up);
        }

        if (uIController == null)
        {
            uIController = gameObject.GetComponent<UIController>();

            IUpdatable up = uIController as IUpdatable;
            if (up != null)
                Updatables.Add(up);
        }

        if (objectPoolSystem == null)
        {
            objectPoolSystem = gameObject.GetComponent<ObjectPoolSystem>();

            IUpdatable up = objectPoolSystem as IUpdatable;
            if (up != null)
                Updatables.Add(up);
        }
    }
	
	// Update is called once per frame
	public override void UpdateMe()
    {
        float deltaTime = Time.deltaTime;

        for(int i = 0; i < Updatables.Count; i++)
        {
            Updatables[i].UpdateDelta(deltaTime);
        }
	}

    public override void FixedUpdateMe()
    {
        float deltaTime = Time.fixedDeltaTime;

        for (int i = 0; i < Updatables.Count; i++)
        {
            Updatables[i].UpdateFixedDelta(deltaTime);
        }
    }

    public override void LateUpdateMe()
    {
        float deltaTime = Time.deltaTime;

        for (int i = 0; i < Updatables.Count; i++)
        {
            Updatables[i].UpdateLateDelta(deltaTime);
        }
    }

    /// <summary>
    /// Добавление объекта в список обновляемых объектов 
    /// </summary>
    public void AddUpdatable(IUpdatable up)
    {
        if (up != null)
        {
            Updatables.Add(up);
        }
    }

    /// <summary>
    /// Удаление объекта из списка обновляемых объектов 
    /// </summary>
    public void RemoveUpdatable(IUpdatable up)
    {
        Updatables.Remove(up);
    }

    public void CreateLobby()
    {
        playerController.ChangeHeroByEntity(objectPoolSystem.GetObj(ObjectType.Entity, heroTeam.HeroName) as Entity); // ставим начального героя 

        AddUpdatable(heroTeam);
    }

    public List<SceneView.OnSceneFunc> GetActions() // Получаем все методы для передачи в EditorWindow
    {
        Undo.RecordObject(this, "GMEW opened");
        
        if (heroTeam == null)
            heroTeam = new HeroTeam();

        List<SceneView.OnSceneFunc> actions = new List<SceneView.OnSceneFunc>();
        
        actions.Add(heroTeam.DrawWayLines);
        
        return actions;
    }

}


/// <summary>
/// Данный объект полностью управляется из GameManager
/// </summary>
[Serializable]
public class Team : IUpdatable
{
    [Serializable]
    public class Spawner
    {
        [Serializable]
        public class WayPath
        {
            public Transform[] points; // Точки пути
        }

        public Spawner(string ID, int SpawnCount, int SpawnDelay)
        {
            Identifier = ID;
            this.SpawnCount = SpawnCount;
            this.spawnDelay = this.SpawnDelay = SpawnDelay;
        }

        /// <summary>
        /// Идентификатор сущности (крипа)
        /// </summary>
        public string Identifier; // Идентификатор сущности в пуле
        public int PathIndex;  // Индекс маршрута

        public int SpawnCount; // Количество крипов за 1 спавн
        public int SpawnDelay; // Интервалы между созданием сущностей
        [NonSerialized]
        public int spawnDelay;
        
    }

    [Serializable]
    public class WayPath
    {
        public Transform[] points; // Точки пути
        public Vector3[] vectorPoints;


        public void InitVectorPoints()
        {
            vectorPoints = new Vector3[points.Length];
            for(int i = 0; i < points.Length; i++)
            {
                vectorPoints[i] = points[i].position;
            }
        }
    }



    public List<Entity> entities = new List<Entity>();
    public string TeamName { get; set; }

    [Tooltip("Массив маршрутов для спавнеров")]
    public WayPath[] Paths;    // Пути
    [Tooltip("Спавнеры")]
    public Spawner[] Spawners; // Спавнеры сущностей

    public virtual void UpdateDelta(float deltaTime)
    {
        for(int i = 0; i < Spawners.Length; i++)
        {
            Spawner sp = Spawners[i];

            sp.spawnDelay -= (int)deltaTime;

            if (sp.spawnDelay <= 0)
            {
                for(int j = 0; j < sp.SpawnCount; j++)
                {
                    Entity entity = ObjectPoolSystem.PoolSys.GetObj(ObjectType.Entity, sp.Identifier) as Entity;
                    entity.transform.position = Paths[sp.PathIndex].points[0].position;
                    entity.RestoreStats();
                    entity.SetWayPoints(Paths[sp.PathIndex].points);
                }
            }
        }
    }

    public virtual void UpdateFixedDelta(float deltaTime)
    {
        
    }

    public virtual void UpdateLateDelta(float deltaTime)
    {
       
    }

    public void DrawWayLines(SceneView sceneView)
    {

        for (int i = 0; i < Paths.Length; i++)
        {
            Transform[] points = Paths[i].points;
            int arrayLength = points.Length - 2;

            for (int j = 0; j <= arrayLength; j++)
            {
               

                Handles.color = Color.green;
                Handles.DrawDottedLine(points[j].position, points[j + 1].position,4f);
            }
        }
        
    }
}


[Serializable]
public class HeroTeam: Team
{
    public string HeroName;
}

[Serializable]
public class EnemyTeam: Team
{
    /*
     * Здесь будут настройки
     * 
     * 
     */
}
