﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class Item : ScriptableObject {

    public string Name;

    public short Cost;
    public short Weight;

    public abstract void Use(Entity sender);

}

public class Scroll : Item
{
    public Skill skill; 
    public override void Use(Entity sender)
    {
        // Здесь мы создаем окно амены скилов

        // sender.Abilities.Add(skill);
    }
}
