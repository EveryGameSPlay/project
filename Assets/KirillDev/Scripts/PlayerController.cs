﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : OverridableMonoBehaviour, IUpdatable{

    public static PlayerController pc;

    public CameraFollow MainCamera;   // Главная используемая камера
    public UIController uiController; // Компонент отрисовки интерфейса

    //public GameObject targetLock; // Спрайт выбранной цели 
    public GameObject highLight;  // Выбранный герой

    public string TeamName;  // Название команд
    public Entity startHero; // Стартовый герой
    public List<Entity> heroes = new List<Entity>(); // Персонажи управляемые игроком
    private int currentIndex = 0; // Индекс выбранного героя в списке

    public Entity currentHero;    // Выбранный герой
    public Entity CurrentHero 
    {
        get { return currentHero; }
        set
        {
            uiController.ChangeMainSkill(-1, InputSkillButton); // Возвращаем часть рендера в обычное состояние
            InputSkillButton = -1;

            currentHero = value;
            LastEntity = currentHero;
            uiController.SubscribeToEntity(currentHero);
            uiController.AddUnitBox(currentHero);

            for (int i = 0; i < heroes.Count; i++)
            {
                if (currentHero == heroes[i]) // Если сущность уже добавлена
                    return;
            }


            currentHero.Team = TeamName;
            currentHero.UnderControll = true;
            AddHero(currentHero);
            currentHero.GroupControll = false;
            
            
        }
    }
    
    private Entity lastEntity;
    public  Entity LastEntity {
        get { return lastEntity; }
        set
        {
            lastEntity = value;
            currentHero.Target = lastEntity;
            //targetLock.SetActive(true);
            //targetLock.transform.position = lastEntity.transform.position
            
        }
    } // Последнee выбранное существо 

    private int inputskillbutton; // Последнее активное заклинание  (-1 == false)
    private int InputSkillButton
    {
        get { return inputskillbutton; }
        set
        {
            if (value == -1)
            {
                inputskillbutton = -1;
                uiController.ChangeMainSkill(value, inputskillbutton);
                return;
            }

            if (inputskillbutton == value)
            {
                inputskillbutton = -1;
                uiController.ChangeMainSkill(-1, value);
                return;
            }

            if (CurrentHero.MainAbilities[value] != null) // Если скилл существует
            {
                uiController.ChangeMainSkill(value, inputskillbutton);
                inputskillbutton = value;
                return;
            }

            inputskillbutton = -1;
        }

    } // Если нажата кнопка скила

    private bool inputrightmouse;  // Если нажата правая кнопка мыши
    private bool InputRightMouse
    {
        get { return inputrightmouse; }
        set
        {
            inputrightmouse = value;

            if (inputrightmouse == true)
            {
                if(CurrentHero != null)
                {
                    CameraRayEntityTarget();
                }

                CameraRayGround();
            }
        }

    }

    private bool inputleftmouse; 
    private bool InputLeftMouse
    {
        get { return inputleftmouse; }
        set
        {
            inputleftmouse = value;

            if (inputleftmouse == true) 
            {
                if (InputSkillButton == -1) // Если мы не хотим использовать способность
                {
                    CameraRayEntity();
                }
                else
                {
                    
                    CameraRayEntitySkillTarget();
                }
            }
        }

    }  // Если нажата левая  кнопка мыши
    
    private bool inputherokey;     // Если нажата кнопка h (центрируемся на герое)
    private bool InputHeroKey
    {
        get { return inputherokey; }
        set
        {
            inputherokey = value;
            if(inputherokey == true)
            {
                CurrentHero.ICF_Clicked = true;
                lastEntity = CurrentHero;
                CameraRequest(CurrentHero,true);
               
            }
        }

    }

    private bool inputcentrekey;   // Если нажата кнопка C
    private bool InputCentreKey
    {
        get { return inputcentrekey; }
        set
        {
            inputcentrekey = value;

            if (inputcentrekey == true)
            {
                CameraRayICF();
            }
        }
    }




    protected override void Awake()
    {
        pc = this;

        base.Awake();
    }

    // Use this for initialization
    void Start () {
        
        CurrentHero = startHero;
        CurrentHero.ICF_Clicked = true;
        CameraRequest(CurrentHero, true); // Передвигаем камеру на персонажа

        InputSkillButton = -1;

        Awake(); // Для 100% добавления в менеджер
    }


    public void UpdateDelta(float deltaTime)
    {
        if (Input.GetKeyDown(KeyCode.Q)) // Смена героя по списку
        {
            ChangeHero();
        }

        if (CurrentHero == null || !CurrentHero.gameObject.activeSelf || !CurrentHero.Controllable) // Меняем героя, если прошлый больше не под контролем
        {
            print("NULLORELSE ENTITY");
            heroes.Remove(CurrentHero);

            if (!ChangeHero()) // Если нет ни одного героя
            {
                TryChangeHero();
                return;
            }
        }

        if (InputSkillButton != -1 && CurrentHero.MainAbilities[InputSkillButton] == null) // Если способность была удалена
            InputSkillButton = -1;

        #region Skill Input
        if (Input.GetKeyDown(KeyCode.Alpha1)) // Кнопка умения
            InputSkillButton = 0;

        if (Input.GetKeyDown(KeyCode.Alpha2)) // Кнопка умения
            InputSkillButton = 1;

        if (Input.GetKeyDown(KeyCode.Alpha3)) // Кнопка умения
            InputSkillButton = 2;

        if (Input.GetKeyDown(KeyCode.Alpha4)) // Кнопка умения
            InputSkillButton = 3;

        if (Input.GetKeyDown(KeyCode.Alpha5)) // Кнопка умения
            InputSkillButton = 4;
        #endregion

        InputLeftMouse = Input.GetKeyDown(KeyCode.Mouse0);
        InputRightMouse = Input.GetKeyDown(KeyCode.Mouse1);
        InputCentreKey = Input.GetKeyDown(KeyCode.C);
        InputHeroKey = Input.GetKeyDown(KeyCode.H);

        highLight.transform.position = CurrentHero.transform.position - new Vector3(0, CurrentHero.Height);
    }

    public void TryChangeHero()
    {
        InputLeftMouse = Input.GetKeyDown(KeyCode.Mouse0);
    }

    public void UpdateFixedDelta(float deltaTime)
    {

    }

    public void UpdateLateDelta(float deltaTime)
    {
        uiController.RenderSelectedEntity(lastEntity);
    }

    // Если объект сам вызвал эту функцию и хочет центрировать камеру
    public void CameraRequest(ICameraFollow oneOfQueue,bool camKey) // camKey - кнопка отвечающая за запросы с клавиатуры игрока
    {

        if (!oneOfQueue.ICF_Event) // Если этот объект не событие
        {
            if (InputSkillButton == -1 && camKey) // Если не нажата кнопка способности и нажата кнопка C
            {
                MainCamera.SetTarget(oneOfQueue);
            }
        }
        else // Если объект событие
        {
            MainCamera.SetTarget(oneOfQueue);
        }

    }

    // Получаем объект ICameraFollow через рейкаст
    private void CameraRayICF()
    {
        RaycastHit hit;
        Ray ray = MainCamera.camera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            ICameraFollow objectHit = hit.transform.GetComponent<ICameraFollow>();

            if(objectHit != null)
            {
                print("Hit_ICF");
                objectHit.ICF_Clicked = true;
                CameraRequest(objectHit,true);
            }
            // Do something with the object that was hit by the raycast.
        }
    }

    // Получаем точку на земле куда кликнул игрок
    private void CameraRayGround()
    {
        RaycastHit hit;
        Ray ray = MainCamera.camera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            GameObject objectHit = hit.transform.gameObject;

            if (objectHit.tag == "Ground")
            {
                CurrentHero.MoveToPosition(hit.point);
            }
            
        }
    }

    // Получаем сущность куда кликнул игрок   //LMK
    private void CameraRayEntity()
    {
        RaycastHit hit;
        Ray ray = MainCamera.camera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            Entity objectHit = hit.transform.GetComponent<Entity>();

            if (objectHit == null)
                return;

            LastEntity = objectHit; // Ставим последнего выбранного

            if (!objectHit.Controllable)
                return;

            ChangeHeroByEntity(objectHit);
           
        }
    }

    // Получает цель для сущности героя
    private void CameraRayEntityTarget()
    {
        RaycastHit hit;
        Ray ray = MainCamera.camera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            Entity objectHit = hit.transform.GetComponent<Entity>();

            if (objectHit == CurrentHero)
                return;

            CurrentHero.Target = objectHit;
            
        }
    }

    // Получает цель для сущности героя и применяет способность
    private void CameraRayEntitySkillTarget()
    {
        RaycastHit hit;
        Ray ray = MainCamera.camera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            Entity objectHit = hit.transform.GetComponent<Entity>();

            if (objectHit == CurrentHero || objectHit == null)
                return;

            CurrentHero.Target = objectHit;
            CurrentHero.UseSkill(InputSkillButton);

        }
    }

    // Меняем основного героя
    public bool ChangeHero()
    {
        if (heroes.Count <= 0)
            return false;

        for (int i = heroes.Count; currentIndex < heroes.Count; currentIndex++,i--)
        {
            if (i >= 0)
            {
                if (currentIndex >= heroes.Count)
                    currentIndex = 0;

                if (heroes[currentIndex] == null)
                {
                    RemoveHero(heroes[currentIndex]);
                }
                else
                {
                    break;
                }
            }
            else
            {
                return false;
            }
        }

        CurrentHero.UnderControll = false;
        CurrentHero = heroes[currentIndex];
        CurrentHero.UnderControll = true;
   
        CurrentHero.ICF_Clicked = true;
        CameraRequest(CurrentHero,true); // Передвигаем камеру на персонажа
        return true;
    }

    public void ChangeHeroByEntity(Entity newHero)
    {
        CurrentHero.UnderControll = false;
        CurrentHero = newHero;
        CurrentHero.UnderControll = true;

        CurrentHero.ICF_Clicked = true;
        CameraRequest(CurrentHero, true); // Передвигаем камеру на персонажа
    }

    public void EnableEntityControll(Entity entity)
    {
        
        entity.GroupControll = true;
    }

    public void DisableEntityControll(Entity entity)
    {
        entity.GroupControll = false;
    }

    public void AddHero(Entity newHero)
    {
        newHero.GroupControll = true;
        heroes.Add(newHero);
    }

    public void RemoveHero(Entity entity) // Не проверена
    {
        heroes.Remove(entity);
    }
    
}
