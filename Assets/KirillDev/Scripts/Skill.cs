﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Существует несколько видов скилов:
 * Пассивные - реагируют на внешние события (Нужно подписывать)
 * Активные  - запускают внешние события (Подписывать необязательно)
*/

public abstract class Skill : ScriptableObject, IRandomChance {

    public SkillInfo Info; // Информация о способности
    
    public float LifeCost; // Затраты на использование
    public float ManaCost;
    public float AgilCost;

    public float DistToHit; // Максимальная дистанция до цели 
    
    public ParticleSystem Particle; // Эффект скила

    public bool NullableTarget; // Может ли цель быть Null
    protected bool AlreadyInUse = false; // Чтобы выйти из бесконечного вызова event
    public int IRC_Chance { get; set; }


    public virtual void Use(Entity sender,Entity defender)
    {
        
    }

    public abstract void Subscribe(Entity owner);

    //public virtual bool CheckDist(float distance)
    //{
    //    if(distance <= DistToHit) // Если мы на достаточном расстоянии 
    //    {
    //        return true;
    //    }

    //    return false;
    //}


}

