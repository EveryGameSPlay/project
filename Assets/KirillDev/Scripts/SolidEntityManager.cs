﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///  Этот объект обновляет тех ботов, которые не были зарегестрированы в EntityManager
/// </summary>
public class SolidEntityManager : EntityManager {

    public static SolidEntityManager SEM;

    protected override void Awake()
    {
        base.Awake();
        SEM = this;
    }
    // Use this for initialization
    public override void Start ()
    {
        base.Start();
        GameManager.gameManager.AddUpdatable(this);
	}

   
}
