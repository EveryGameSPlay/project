﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolTest : OverridableMonoBehaviour {

	// Use this for initialization
	void Start () {
        delay = Delay;
        base.Awake();
	}

    public  float Delay;
    private float delay;

	
	// Update is called once per frame
	public override void UpdateMe () {
		
        if(delay <= 0)
        {
            delay = Delay;

            Hero hero = ObjectPoolSystem.PoolSys.GetObj(ObjectType.Entity, "Skeleton") as Hero;

            hero.gameObject.SetActive(true);
            hero.transform.position = transform.position;

            StartCoroutine(Deacive(hero));
        }
        else
        {
            delay -= Time.deltaTime;
        }

	}

    public IEnumerator Deacive(Entity entity)
    {

        yield return new WaitForSeconds(5f);

        ObjectPoolSystem.PoolSys.AddObj(entity, ObjectType.Entity, "Skeleton");
    }
}
