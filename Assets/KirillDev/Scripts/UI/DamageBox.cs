﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DamageBox: OverridableMonoBehaviour
{
    public Image Initiator;
    public Image Defender;
    public TextMeshProUGUI damageTextLife; // Текст урона
    public TextMeshProUGUI damageTextMana; // Текст урона
    public TextMeshProUGUI damageTextAgil; // Текст урона

    public float currentLifeTime;

    public RectTransform rtransform; 
}

