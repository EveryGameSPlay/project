﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PopupDamage : OverridableMonoBehaviour {

    public TextMeshProUGUI Text;
    public float LifeTime;
    public float Speed; // Скорость
    public float speedRise; // Коэфицент снижения скорости

    public Vector3 SpawnPosition; // Начальная точка спавна
    public Vector3 Direction; // Направление движения

    public RectTransform rtansform;

}


//float angle = 0;
//float speed = (2 * Mathf.PI) / 5 //2*PI in degress is 360, so you get 5 seconds to complete a circle
// float radius = 5;
//void Update()
//{
//    angle += speed * Time.deltaTime; //if you want to switch direction, use -= instead of +=
//    x = Mathf.Cos(angle) * radius;
//    y = Mathf.Sin(angle) * radius;
//}