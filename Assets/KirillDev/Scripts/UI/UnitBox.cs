﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UnitBox : EventTrigger {

    public Entity entity;
    public Image image; // Изображение сущности
    public RectTransform rtransform;



    public override void OnPointerDown(PointerEventData eventData)
    {
        PlayerController.pc.ChangeHeroByEntity(entity);
    }
}
