﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class UIController : OverridableMonoBehaviour, IUpdatable
{

    public static UIController UI;


    public RectTransform buffer;

    private Entity DamageReciever; // Сущность которую слушает DamageBox
   
    [Space]
    [Header("Main Info")]
    public Image photo; // Изображение сущности

    public Image HealthBar; // Полоса жизни
    public TextMeshProUGUI HealthText;
    public Image ManaBar;   // Полоса маны
    public TextMeshProUGUI ManaText;

    public Color SkillColor; // Цвет умения, находящегося в покое
    public Color UsingColor; // Цвет умения, когда его применяют

    public Image[] MainAbilities = new Image[5]; // Основные способности
    public Image[] BornAbilities = new Image[2]; // Врожденные способности


    [Header("Resource")][Space]
    public Canvas MainCanvas;

    public Sprite EmptySkill;  // Спрайт пустой ячейки
    public Image SkillInfoBox; // Панель с информацией о способности

    [Space]
    [Header("Damage List")]
    public Image panel;
    public DamageBox prefab;

    public int BoxCount;       // Количество ячеек
    public float BoxLifeTime;  // Время жизни ячейки
    public float BoxDropSpeed; // Скорость анимации падения
    public float Offest;       // Расстояние между ячейками

    public float MaxRectY; // Максимальное и минимальное значения позиции RectTransform 
    public float MinRectY;
    public List<DamageBox> activeBoxes; // Активированы на данный момент . Последний элемент буффер

    [Space]
    [Header("Damage Popup")]
    public PopupDamage popupPrefab; // Префаб выпадающего урона

    public float PopupLifeTime; // Время жизни текста
    public float PopupSpeed;    // Скорость анимации Popup 
    public float PopupScale;    // Начальный масштаб 
    public float PopupScaleInt; // Интенсивность изменения масштаба

    public Vector3 PopupGravity; // Вектор гравитации

    public int PopupCount;               // Стартовое количество текста
    public Queue<PopupDamage> PopupPool; // Пул Popup объектов (неактивны)
    public List<PopupDamage>  PopupList; // Лист активных Popup объектов

    // Значения изменяемые во время игры
    private int dir = 1; // Зеркальное отображение вектора 
    private float angle  = 0;
    private float radius = 1;
    private float speed  = (2 * Mathf.PI) / 5; //2*PI in degress is 360, so you get 5 seconds to complete a circle
    
    [Space]
    [Header("Units List")]
    public UnitBox unitPrefab;

    public RectTransform unitPanel; // Родительская панель, содержащая все эти элементы
    public int unitBoxCount; // Максимальное количество иконок
    public List<UnitBox> units; // Ячейки с подконтрольными существами


    private void Start()
    {
        UI = this;
        base.Awake();

        PopupPool = new Queue<PopupDamage>();
        PopupList = new List<PopupDamage>();
        for(int i = 0; i < PopupCount; i++)
        {
            PopupDamage pd = Instantiate(popupPrefab, transform.position, transform.rotation,buffer);
            pd.rtansform = pd.gameObject.GetComponent<RectTransform>();
            pd.LifeTime = PopupLifeTime;
            pd.Speed = PopupSpeed;
            pd.Text.text = null;
            pd.gameObject.SetActive(false);
            PopupPool.Enqueue(pd);
        }

        for(int i = 0; i < unitBoxCount; i++)
        {
            
            UnitBox ub = Instantiate(unitPrefab, unitPanel.transform.position, unitPanel.transform.rotation);
            ub.rtransform = ub.gameObject.GetComponent<RectTransform>();
            ub.image = ub.transform.GetComponent<Image>();
            ub.rtransform.SetParent(buffer, false);
            ub.gameObject.SetActive(false);
            units.Add(ub); 
        }
        units.Add(null);

        activeBoxes = new List<DamageBox>(BoxCount+1);
        for (int i = 0; i < BoxCount; i++)
        {
            DamageBox db = Instantiate(prefab, panel.transform.position, panel.transform.rotation, panel.transform);
            db.gameObject.SetActive(true);
            db.rtransform = db.gameObject.GetComponent<RectTransform>();
            db.rtransform.anchoredPosition = new Vector3(0, MaxRectY, 0);
            activeBoxes.Add(db);
        }
        activeBoxes.Add(null);
    }
    
    public void UpdateDelta(float deltaTime)
    {
        for (int i = 0; i < unitBoxCount; i++)
        {
            CheckAndRemoveUnitBox(units[i]);
        }
    }

    public void UpdateFixedDelta(float deltaTime)
    {
       
    }

    public void UpdateLateDelta(float deltaTime)
    {

    }

    public void SubscribeToEntity(Entity toSub)
    {
        DesubscribeToEntity(DamageReciever);
        DamageReciever = toSub;

        toSub.damageEvent += AddDamage; // Подписываем панель с ячейками урона
    }

    public void DesubscribeToEntity(Entity toSub)
    {
        if (DamageReciever != null && !toSub.CheckGetDamageEvent())
        {
            toSub.damageEvent -= AddDamage; // Отписываем панель с ячейками урона
        }
    }

    public void RenderSelectedEntity(Entity toRender)
    {
        RenderPopup();

        photo.sprite = toRender.Info.Avatar; // Утсанавливаем главное изображение

        print("Work render selected entity");

        int currentLife =(int)toRender.soulBeh.currentLife;
        int Life = (int)toRender.soulBeh.Life;
        int currentMana = (int)toRender.soulBeh.currentMana;
        int Mana = (int)toRender.soulBeh.Mana;

       
        HealthText.text = $"{currentLife}"+"/"+$"{Life}";
        ManaText.text = $"{currentMana}" + "/" + $"{Mana}";

        // Ставим изображения главных способностей
        for (int i = 0; i < 5; i++) // 5, т.к. всего максимум 5 скилов
        {
            if (toRender.MainAbilities[i] != null) // Если скилл существует
            {
                MainAbilities[i].sprite = toRender.MainAbilities[i].Info.sprite;
            }
            else
            {
                MainAbilities[i].sprite = EmptySkill;
            }
        }

        // Ставим изображения второстепенных стособностей
        for (int i = 0; i < 2; i++) // 2, т.к. всего максимум 2 скилa
        {
            if (toRender.BornAbilities[i] != null)
            {
                BornAbilities[i].sprite = toRender.BornAbilities[i].Info.sprite;
            }
            else
            {
                BornAbilities[i].sprite = EmptySkill;
            }

        }

        
        
        // Рендер - анимация DamageBox
        for(int i = 0; i < BoxCount; i++)
        {
            DamageBox db = activeBoxes[i];

            if (db.gameObject.activeSelf) // Проверяем активные коробки, все неактивные являются выключенными и ненужными
            {
                db.currentLifeTime -= Time.deltaTime;

                if(db.currentLifeTime <= 0)
                {
                    activeBoxes[BoxCount] = activeBoxes[0];

                    for (int j = 0; j < BoxCount; j++) // Сдвиг влево. Оставнавливаемся на самом последнем, т.к. i+1;
                    {
                        activeBoxes[j] = activeBoxes[j + 1];
                        
                    }

                    db.rtransform.anchoredPosition = new Vector3(0, MaxRectY, 0);
                    db.currentLifeTime = BoxLifeTime;
                    db.gameObject.SetActive(false);
                }

                db.rtransform.anchoredPosition = Vector3.MoveTowards(db.rtransform.anchoredPosition, new Vector2(db.rtransform.anchoredPosition.x, MinRectY + Offest * i), BoxDropSpeed * Time.deltaTime);

            }
           
        }

    }

    public void AddDamage(Entity.DamageInfo DInfo)
    {
        
        for(int i = 0; i < BoxCount; i++) // Есть неактивные ячейки
        {
            if(activeBoxes[i].gameObject.activeSelf == false)
            {
                DamageBox db = activeBoxes[i];
                db.rtransform.anchoredPosition = new Vector3(0, MaxRectY, 0);
                db.currentLifeTime = BoxLifeTime;

                db.gameObject.SetActive(true);

                RenderDamageBox(db, DInfo);

                return; // Выходим из метода, т.к. все утсановили
            }
        }

        // Сюда мы попадаем если все ячейки активны
        DamageBox dbswap = activeBoxes[0];
        activeBoxes[BoxCount] = dbswap;

        for(int i = 0; i < BoxCount; i++) // Сдвиг влево. Оставнавливаемся на самом последнем, т.к. i+1;
        {
            activeBoxes[i] = activeBoxes[i + 1];
            
        }

        dbswap.rtransform.anchoredPosition = new Vector3(0, MaxRectY, 0);
        dbswap.currentLifeTime = BoxLifeTime;

        RenderDamageBox(dbswap, DInfo);

        dbswap.gameObject.SetActive(true);


    }

    private void RenderDamageBox(DamageBox db, Entity.DamageInfo DInfo)
    {
        db.Initiator.sprite = DInfo.Intiator.Info.Avatar;
        db.Defender.sprite = DInfo.Defender.Info.Avatar;
        db.damageTextLife.text = DInfo.sumLifeDamage.ToString("F0");
        db.damageTextMana.text = DInfo.sumManaDamage.ToString("F0");
        db.damageTextAgil.text = DInfo.sumAgilDamage.ToString("F0");
    }

    //-----------------------------------------UNIT_BOX
    public void AddUnitBox(Entity entity)
    {
        
        for (int i = 0; i < unitBoxCount; i++)
        {
            
            if (entity == units[i].entity) // Если сущность уже добавлена
                return;
        }

        for (int i = 0; i < unitBoxCount; i++) // Есть неактивные ячейки
        {
            if (units[i].gameObject.activeSelf == false)
            {
                UnitBox ub = units[i];
                ub.transform.SetParent(unitPanel, false); 

                ub.gameObject.SetActive(true);
                
                RenderUnitBox(ub,entity);

                return; // Выходим из метода, т.к. все утсановили
            }
            
        }

        // Сюда мы попадаем если все ячейки активны
        UnitBox ubswap = units[0];
        units[unitBoxCount] = ubswap;
        
        for (int i = 0; i < unitBoxCount; i++) // Сдвиг влево. Оставнавливаемся на самом последнем, т.к. i+1;
        {
            units[i] = units[i + 1];
        }

        ubswap.rtransform.SetParent(unitPanel, false);

        RenderUnitBox(ubswap,entity);
        ubswap.gameObject.SetActive(true);
    }

    private void CheckAndRemoveUnitBox(UnitBox ub) // Проверяем, а после удаляем если нужно
    {
        if(ub.entity == null || !ub.entity.gameObject.activeSelf || !ub.entity.Controllable)
        {
            ub.entity = null;
            ub.rtransform.SetParent(buffer, false); // Убираем из ValueGroup в пустышку
            ub.gameObject.SetActive(false);
        }
    }

    private void RenderUnitBox(UnitBox ub, Entity entity)
    {
        ub.entity = entity;
       
        ub.image.sprite = entity.Info.Avatar;
    }

    //-----------------------------------------POPUP_TEXT

    public void AddPopup(float value, Vector3 pos) // Добавить позицию спавна !!!
    {
        if(angle >= 360)
        {
            angle = 0;
        }

        angle += speed; //if you want to switch direction, use -= instead of +=
        Vector3 direction = new Vector3(Mathf.Cos(angle) * radius, 0, Mathf.Sin(angle) * radius);
        direction *= dir *= -1;
        PopupGravity *= dir;
        PopupDamage pd = PopupPool.Dequeue();
        
        pd.SpawnPosition = pos;
        pd.speedRise = 0;
        pd.Speed = PopupSpeed;
        pd.Direction = direction;
        pd.LifeTime = PopupLifeTime;
        pd.Text.text = value.ToString("F0");
        pd.rtansform.localEulerAngles = new Vector3(0, 0, 0);
        pd.transform.localScale = Vector3.one * PopupScale;
        pd.gameObject.SetActive(true);

        PopupList.Add(pd);
    }

    private void RenderPopup() // Полное управление всеми Popup (движение, масштаб, текст)
    {

        for (int i = 0; i < PopupList.Count; i++)
        {
            PopupDamage pd = PopupList[i];

            if(pd.LifeTime <= 0)
            {
                RemovePopup(pd);
            }
            
            Vector3 textPosition = Camera.main.WorldToScreenPoint(pd.SpawnPosition);  //  Find the 2D position of the object using the main camera

            pd.Direction = (Quaternion.Euler(PopupGravity.x, PopupGravity.y, PopupGravity.z) * pd.Direction).normalized;

            if (pd.Speed >= 0)
            {
                pd.Speed -= Time.deltaTime * (pd.speedRise += 1.5f);
            }else
            {
                pd.Speed = 0.1f;
            }

            if (pd.transform.localScale.magnitude >= 0.1f)
                pd.transform.localScale -= Vector3.one * PopupScaleInt * Time.deltaTime;

            pd.SpawnPosition += pd.Direction.normalized * pd.Speed * Time.deltaTime;

            pd.transform.position = textPosition;
          
            pd.LifeTime -= Time.deltaTime;

        }


    }

    private void RemovePopup(PopupDamage pd)
    {
        pd.gameObject.SetActive(false);
        PopupList.Remove(pd);

        PopupPool.Enqueue(pd);
    }



    public void CreateSkillInfoBox(PointerEventData pointer,int skillCategory, int skillIndex)
    {
        var m_DraggingPlane = MainCanvas.transform as RectTransform;
        var rt = SkillInfoBox.GetComponent<RectTransform>();

        Vector3 globalMousePos;
        if (RectTransformUtility.ScreenPointToWorldPointInRectangle(m_DraggingPlane, pointer.position, pointer.pressEventCamera, out globalMousePos))
        {
            rt.position = globalMousePos;
            rt.rotation = m_DraggingPlane.rotation;
        }
    }

    public void ChangeMainSkill(int index, int lastindex) // Меняет цвет изображения способности
    {
        if(index != -1) // Если мы меняем способность
        {
            if(lastindex != -1) // Если были включены предыдущая и настоящая
            {
                MainAbilities[lastindex].color = SkillColor;
                MainAbilities[index].color = UsingColor;
            }
            else
            {
                MainAbilities[index].color = UsingColor;
            }

        }
        else
        {
            if(lastindex != -1)
            {
                MainAbilities[lastindex].color = SkillColor;
            }
        }
    }
    
}
