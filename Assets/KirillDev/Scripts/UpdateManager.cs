﻿using UnityEngine;

public class UpdateManager : MonoBehaviour
{
    private static UpdateManager instance;

    public int regularUpdateArrayCount = 0;
    public int fixedUpdateArrayCount = 0;
    public int lateUpdateArrayCount = 0;

    public OverridableMonoBehaviour[] regularArray = new OverridableMonoBehaviour[0];
    public OverridableMonoBehaviour[] fixedArray = new OverridableMonoBehaviour[0];
    public OverridableMonoBehaviour[] lateArray = new OverridableMonoBehaviour[0];

    //We're setting it to true before any for loops. We're setting it to false ater the for loops. We're then checking it for being true somewhere next.
    //If we spot it being true - we know that an exception happened and we should execute a fix.
    private bool exceptionCatched;



    public void Awake()
    {
        if (Time.time > 1f)
        {
            Destroy(this.gameObject);
        }
        else
            instance = this;

        //Debug.Log(instance);
    }

    public static void AddItem(OverridableMonoBehaviour behaviour)
    {
        //awakes are called randomly across all the game objects. There may be an issue when an object calls to subscribe only to throw an exception. 
        //We're doing this just for a double-check. This will not be encountered in builds at all since the UpdateManager is initialized on the game start.
        if (instance == null)
        {
            instance = GameObject.Find("Rewired Input Manager")?.GetComponent<UpdateManager>();
        }
        
        if (instance != null && behaviour != null)
        instance.AddItemToArray(behaviour);
    }

    public static void RemoveSpecificItem(OverridableMonoBehaviour behaviour)
    {
        if (instance == null)
        {
            instance = GameObject.Find("Rewired Input Manager")?.GetComponent<UpdateManager>();
        }

        if (behaviour && instance)
            instance.RemoveSpecificItemFromArray(behaviour);
    }

    public static void RemoveSpecificItemAndDestroyIt(OverridableMonoBehaviour behaviour)
    {
        instance.RemoveSpecificItemFromArray(behaviour);

        Destroy(behaviour.gameObject);
    }

    private void AddItemToArray(OverridableMonoBehaviour behaviour)
    {
        if (!CheckIfArrayContainsItem(regularArray, behaviour) && behaviour.GetType().GetMethod("UpdateMe").DeclaringType != typeof(OverridableMonoBehaviour))
        {
            regularArray = ExtendAndAddItemToArray(regularArray, behaviour);
            regularUpdateArrayCount++;
        }

        if (!CheckIfArrayContainsItem(fixedArray, behaviour) && behaviour.GetType().GetMethod("FixedUpdateMe").DeclaringType != typeof(OverridableMonoBehaviour))
        {
            fixedArray = ExtendAndAddItemToArray(fixedArray, behaviour);
            fixedUpdateArrayCount++;
        }

        if (!CheckIfArrayContainsItem(lateArray, behaviour) && behaviour.GetType().GetMethod("LateUpdateMe").DeclaringType != typeof(OverridableMonoBehaviour))
        {
            lateArray = ExtendAndAddItemToArray(lateArray, behaviour);
            lateUpdateArrayCount++;
        }

        ArrayExceptionSolver();
        //Debug.Log(behaviour + " / " + behaviour.gameObject);
        //Debug.Log(System.Environment.StackTrace);
        return;
    }

    public OverridableMonoBehaviour[] ExtendAndAddItemToArray(OverridableMonoBehaviour[] original, OverridableMonoBehaviour itemToAdd)
    {
        int size = original.Length;

        for (int i = 0; i < size; i++)
        {
            if (original[i] == null)
            {
                original[i] = itemToAdd;
                return original;
            }
        }

        OverridableMonoBehaviour[] finalArray = new OverridableMonoBehaviour[size + 1];
        for (int i = 0; i < size; i++)
        {
            finalArray[i] = original[i];
        }
        finalArray[finalArray.Length - 1] = itemToAdd;
        return finalArray;
    }

    private void RemoveSpecificItemFromArray(OverridableMonoBehaviour behaviour)
    {
        //Debug.Log("Removal Called   " + behaviour + "   " + behaviour.gameObject);
        if (CheckIfArrayContainsItem(regularArray, behaviour))
        {
            regularArray = ShrinkAndRemoveItemToArray(regularArray, behaviour);
            regularUpdateArrayCount--;
        }

        if (CheckIfArrayContainsItem(fixedArray, behaviour))
        {
            fixedArray = ShrinkAndRemoveItemToArray(fixedArray, behaviour);
            fixedUpdateArrayCount--;
        }

        if (CheckIfArrayContainsItem(lateArray, behaviour))
        {
            lateArray = ShrinkAndRemoveItemToArray(lateArray, behaviour);
            lateUpdateArrayCount--;
        }

        ArrayExceptionSolver();
        return;
    }

    public bool CheckIfArrayContainsItem(OverridableMonoBehaviour[] arrayToCheck, OverridableMonoBehaviour objectToCheckFor)
    {
        int size = arrayToCheck.Length;

        for (int i = 0; i < size; i++)
        {
            if (objectToCheckFor == arrayToCheck[i]) return true;
        }

        return false;
    }

    public OverridableMonoBehaviour[] ShrinkAndRemoveItemToArray(OverridableMonoBehaviour[] original, OverridableMonoBehaviour itemToRemove)
    {
        int size = original.Length;
        if (size == 0) size = 1;
        OverridableMonoBehaviour[] finalArray = new OverridableMonoBehaviour[size - 1];

        int virtI = 0;
        if (finalArray.Length != 0)
        {
            for (int i = 0; i < size; i++)
            {
                if (original[i] == itemToRemove)
                {
                    original[i] = null;
                    virtI++;
                }
                else finalArray[i - virtI] = original[i];
            }
        }
        return finalArray;
    }

    public void ArrayExceptionSolver() //solves the array.length not being equal to integer length exception (since deliberately calling Array.length is expensive). Happens rarely on scene loads, so no big deal.
    {
        //Debug.Log("ExceptionCatched!");
        //Debug.Log(System.Environment.StackTrace);
        regularUpdateArrayCount = regularArray.Length;
        lateUpdateArrayCount = lateArray.Length;
        fixedUpdateArrayCount = fixedArray.Length;

        if (regularUpdateArrayCount < 0) regularUpdateArrayCount = 0;
        if (lateUpdateArrayCount < 0) lateUpdateArrayCount = 0;
        if (fixedUpdateArrayCount < 0) fixedUpdateArrayCount = 0;
    }


    private void Update()
    {
        if (exceptionCatched)
        {
            ArrayExceptionSolver();
        }
        if (regularUpdateArrayCount == 0) return;

        exceptionCatched = true;

        for (int iOrder = -10; iOrder < 11; iOrder++)
        {
            for (int i = 0; i < regularUpdateArrayCount; i++)
            {
                    if (iOrder == regularArray[i].internal_ExecutionOrder)
                    {
                        if (regularArray[i].internal_Enabled)
                            regularArray[i].UpdateMe();
                    }
            }
        }

        exceptionCatched = false;
    }

    private void FixedUpdate()
    {
        if (exceptionCatched)
        {
            ArrayExceptionSolver();
        }

        if (fixedUpdateArrayCount == 0) return;

        exceptionCatched = true;

        for (int i = 0; i < fixedUpdateArrayCount; i++)
        {
            if (fixedArray[i] != null && fixedArray[i].internal_Enabled)
                fixedArray[i].FixedUpdateMe();
        }
        exceptionCatched = false;
    }

    private void LateUpdate()
    {
        if (exceptionCatched)
        {
            ArrayExceptionSolver();
        }

        if (lateUpdateArrayCount == 0) return;

        exceptionCatched = true;

        for (int i = 0; i < lateUpdateArrayCount; i++)
        {
            if (lateArray[i] != null && lateArray[i].internal_Enabled)
                lateArray[i].LateUpdateMe();
        }
        exceptionCatched = false;
    }
}











