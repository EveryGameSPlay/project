﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Dark", menuName = "Skill/DarkBall")]
public class DarkBallSkill : Skill
{
    public float TargetLifeDamage;
    public float TargetManaDamage;

    public override void Use(Entity sender,Entity defender) // Наносим урон засчет своего хп и маны
    {
        if (sender.soulBeh.Mana >= ManaCost)
        {
            AlreadyInUse = true;

            sender.GetLifeDamage(new Entity.DamageInfo { LifeDamage = LifeCost }); // Наносим себе урон
            sender.Target.GetLifeDamage(new Entity.DamageInfo { LifeDamage = TargetLifeDamage }); // Наносим урон врагу

            sender.Target.GetManaDamage(new Entity.DamageInfo { ManaDamage = TargetManaDamage }); // Сжигаем ману врага
            sender.GetManaDamage(new Entity.DamageInfo { LifeDamage = ManaCost }); // Забираем ману

            AlreadyInUse = false;
        }
        base.Use(sender,defender);
    }

    public override void Subscribe(Entity owner)
    {
        // Подписываемся на событие какое-либо, а можем и не подписываться
    }
}
