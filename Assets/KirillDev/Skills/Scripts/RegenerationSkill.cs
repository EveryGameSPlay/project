﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Regen", menuName = "Skill/Regeneration")]
public class RegenerationSkill : Skill
{
    public float LifeRegen;

    public override void Use(Entity sender, Entity defender) // Хил засчет маны
    {
        if (sender.soulBeh.Mana >= ManaCost)
        {
            AlreadyInUse = true;

            sender.LifeR(LifeRegen);
            sender.GetManaDamage(new Entity.DamageInfo { ManaDamage = ManaCost });

            AlreadyInUse = false;
        }

        base.Use(sender,defender);
    }

    public override void Subscribe(Entity owner)
    {
        // Подписываемся на событие какое-либо, а можем и не подписываться
    }
}
