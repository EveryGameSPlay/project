﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Raplacer", menuName = "Skill/Replacer")]
public class ReplaserSkill : Skill
{
    public float MinDamageToReplace; // Сделать вместо float класс с информацией и ссылкой на наносителя урона

    public override void Use(Entity sender, Entity defender)
    {
        if (sender.soulBeh.Mana >= ManaCost)
        {
            if (sender.Target != null) // Если есть цель, то меняем местами
            {
                AlreadyInUse = true;

                Vector3 pos = sender.transform.position;
                sender.transform.position = sender.Target.transform.position;
                sender.Target.transform.position = pos;

                sender.GetManaDamage(new Entity.DamageInfo { ManaDamage = ManaCost });

                AlreadyInUse = false;
            }
        }
        base.Use(sender,defender);
    }

    public void PassiveUse(Entity.DamageInfo DInfo,Entity sender) // Пассивное срабатывание от полученного урона
    {
        if (DInfo.LifeDamage >= MinDamageToReplace)
        {
            AlreadyInUse = true;

            Vector3 pos = sender.transform.position;
            sender.transform.position = sender.Target.transform.position;
            sender.Target.transform.position = pos;

            AlreadyInUse = false;
        }
    }

    public override void Subscribe(Entity owner)
    {
        owner.LifeDamage += PassiveUse;
    }
}
