﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UnityEngine;

[CreateAssetMenu(fileName = "New Sacrifice", menuName = "Skill/Sacrifice")]
public class SacrificeSkill:Skill
{
    public float TargetLifeDamage;

    public override void Use(Entity sender, Entity defender) // Восстанавливаем хп засчет убийства цели 
    {
        AlreadyInUse = true;

        Entity.DamageInfo dinfo = new Entity.DamageInfo()
        {
            LifeDamage = TargetLifeDamage,
            ManaDamage = TargetLifeDamage,
            AgilDamage = TargetLifeDamage,
            Intiator = sender,
            Defender = defender
        };

        sender.LifeR(defender.soulBeh.currentLife);

        defender.GetLifeDamage(dinfo); // Наносим урон врагу
        
        AlreadyInUse = false;

        base.Use(sender, defender);
    }

    public override void Subscribe(Entity owner)
    {
        
    }


}
