﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New SkillInfo", menuName = "Skill/Info")]
public class SkillInfo : ScriptableObject
{
    public string Name;  // Название скила

    public Sprite sprite; // Изображение способности
    public string Short; // Краткая информация
}

