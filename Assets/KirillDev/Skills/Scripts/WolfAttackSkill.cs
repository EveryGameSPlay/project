﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "New Wolf", menuName = "Skill/WolfAttcak")]
public class WolfAttackSkill : Skill{


    public float TargetLifeDamage;
    public float TargetManaDamage;

    public override void Use(Entity sender,Entity defender) // Наносим урон засчет своего хп и маны
    {

        AlreadyInUse = true;

        Entity.DamageInfo dinfo = new Entity.DamageInfo() {
            LifeDamage = TargetLifeDamage * sender.soulBeh.currentStrength * Random.Range(0.5f, 1f),
            ManaDamage = TargetManaDamage + Random.Range(0, 50),
            AgilDamage = Random.Range(0, 100),
            Intiator = sender,
            Defender = defender
        };
        
        defender.GetLifeDamage(dinfo); // Наносим урон врагу
       

        AlreadyInUse = false;

        base.Use(sender,defender);
    }

    public override void Subscribe(Entity owner)
    {
        // Подписываемся на событие какое-либо, а можем и не подписываться
    }
}
