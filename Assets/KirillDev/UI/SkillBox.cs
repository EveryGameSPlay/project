﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SkillBox : EventTrigger{

    public UIController uiController;

    public int Category; // 0-main, 1-born, 2-baff
    public int Index;

    public override void OnPointerEnter(PointerEventData eventData)
    {
        uiController.CreateSkillInfoBox(eventData,Category,Index);
    }

    public override void OnPointerExit(PointerEventData eventData)
    {
        base.OnPointerExit(eventData);
    }

}
